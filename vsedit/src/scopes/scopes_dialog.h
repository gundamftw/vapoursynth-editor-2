#ifndef SCOPES_DIALOG_H
#define SCOPES_DIALOG_H

#include <QDialog>

namespace Ui {
class ScopesDialog;
}

class FramePainter;
class SettingsManager;

class ScopesDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ScopesDialog(SettingsManager * a_pSettingsManager, QWidget *parent = nullptr);
    ~ScopesDialog() override;

    void setScope(const QPixmap& a_scopePixmap);

    int scopeNum();

protected:

    void showEvent(QShowEvent * a_pEvent) override;
    void moveEvent(QMoveEvent * a_pEvent) override;
    void hideEvent(QHideEvent * a_pEvent) override;
    void closeEvent(QCloseEvent * a_pEvent) override;

    void saveGeometryDelayed();
    void setWindowGeometry();

    QTimer * m_pGeometrySaveTimer;
    QByteArray m_windowGeometry;


private:
    Ui::ScopesDialog *ui;
    SettingsManager * m_pSettingsManager;
//    FramePainter *m_pFramePainter;
    int m_scopeNum;

signals:

    void signalDialogVisibilityChanged(bool);
    void signalScopeChanged(int);

protected slots:

    void slotSaveGeometry();

};

#endif // SCOPES_DIALOG_H
