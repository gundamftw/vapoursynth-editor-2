#include "scopes_dialog.h"
#include "ui_scopes_dialog.h"
#include "../../common-src/settings/settings_manager.h"
#include "../preview/frame_painter.h"

#include <QDebug>
#include <QCloseEvent>
#include <QTimer>

ScopesDialog::ScopesDialog(SettingsManager * a_pSettingsManager, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ScopesDialog),
    m_pSettingsManager(a_pSettingsManager),
    m_scopeNum(1)
{
    ui->setupUi(this);
    setWindowGeometry();

    connect(ui->waveformButton, &QPushButton::clicked, this, [=](){
        m_scopeNum = 1;
        emit signalScopeChanged(m_scopeNum); });

    connect(ui->RGBParadeButton, &QPushButton::clicked, this, [=](){
        m_scopeNum = 2;
        emit signalScopeChanged(m_scopeNum); });
}

ScopesDialog::~ScopesDialog()
{
    delete ui;
}

void ScopesDialog::setScope(const QPixmap &a_scopePixmap)
{
    ui->framePainter->drawFrame(a_scopePixmap);
}

int ScopesDialog::scopeNum()
{
    return m_scopeNum;
}

void ScopesDialog::showEvent(QShowEvent *a_pEvent)
{
    emit signalDialogVisibilityChanged(true);
    QDialog::showEvent(a_pEvent);
}

void ScopesDialog::moveEvent(QMoveEvent *a_pEvent)
{
    QDialog::moveEvent(a_pEvent);
    saveGeometryDelayed();
}

void ScopesDialog::hideEvent(QHideEvent *a_pEvent)
{
    emit signalDialogVisibilityChanged(false);
    QDialog::hideEvent(a_pEvent);
    saveGeometryDelayed();
}

void ScopesDialog::closeEvent(QCloseEvent *a_pEvent)
{
    hide();
    a_pEvent->ignore();
}

void ScopesDialog::saveGeometryDelayed()
{
    QApplication::processEvents();
    if(!isMaximized())
    {
        m_windowGeometry = saveGeometry();
        m_pGeometrySaveTimer->start();
    }
}

void ScopesDialog::setWindowGeometry()
{
    m_pGeometrySaveTimer = new QTimer(this);
    m_pGeometrySaveTimer->setInterval(DEFAULT_WINDOW_GEOMETRY_SAVE_DELAY);
    connect(m_pGeometrySaveTimer, &QTimer::timeout,
        this, &ScopesDialog::slotSaveGeometry);

    m_windowGeometry = m_pSettingsManager->getScopesDialogGeometry();
    if(!m_windowGeometry.isEmpty())
        restoreGeometry(m_windowGeometry);
}

void ScopesDialog::slotSaveGeometry()
{
    m_pGeometrySaveTimer->stop();
    m_pSettingsManager->setScopesDialogGeometry(m_windowGeometry);
}
