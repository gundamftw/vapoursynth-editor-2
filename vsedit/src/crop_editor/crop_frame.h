#ifndef CROPFRAME_H
#define CROPFRAME_H

#include <QGraphicsRectItem>
//#include <QObject>
#include <QWidget>

class CropFrame : public QGraphicsRectItem
{
//    Q_OBJECT
public:
    CropFrame(QWidget *parent = nullptr);

    virtual ~CropFrame() override;

    QRectF boundingRect() const override;

};

#endif // CROPFRAME_H
