#ifndef CROP_EDITOR_DIALOG_H
#define CROP_EDITOR_DIALOG_H

#include <QDialog>
#include <QGraphicsPixmapItem>

namespace Ui {
class CropEditorDialog;
}

class CropEditorDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CropEditorDialog(QWidget *parent = nullptr);
    ~CropEditorDialog() override;

    void setPixmap(const QPixmap &);

protected:
    void hideEvent(QHideEvent * a_pEvent) override;

private:
    Ui::CropEditorDialog *ui;

    QGraphicsScene * m_pCropScene;
    QGraphicsPixmapItem * m_pPixmapItem;

    QImage m_baseImage;

public slots:

signals:

    void signalDialogHidden();
    void signalLoadPixmap();
};

#endif // CROP_EDITOR_DIALOG_H
