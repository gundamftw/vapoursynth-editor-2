#ifndef CROPVIEW_H
#define CROPVIEW_H

#include <QGraphicsView>
#include <QObject>
#include <QWidget>
#include <QGraphicsItem>

class CropFrame;

class CropView : public QGraphicsView
{
    Q_OBJECT
public:
    CropView(QWidget *parent);

protected:
    void mousePressEvent(QMouseEvent * a_pEvent) override;
    void mouseMoveEvent(QMouseEvent * a_pEvent) override;
    void mouseReleaseEvent(QMouseEvent * a_pEvent) override;

private:
    QGraphicsScene * m_crop_area;
    CropFrame *m_frame;

    bool m_leftMouseButtonPressed;
    QPoint m_startPoint;
};

#endif // CROPVIEW_H
