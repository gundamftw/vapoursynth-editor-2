#include "crop_editor_dialog.h"
#include "ui_crop_editor_dialog.h"
#include "crop_scene.h"
#include "crop_view.h"

#include <QGraphicsPixmapItem>
#include <QDebug>

CropEditorDialog::CropEditorDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CropEditorDialog)
{
    ui->setupUi(this);

//    qDebug() << ui->cropView->size().height();
    m_pCropScene = new CropScene(this);
    ui->cropView->setScene(m_pCropScene);

    m_pPixmapItem = new QGraphicsPixmapItem();
    m_pPixmapItem->setFlags(QGraphicsItem::ItemSendsGeometryChanges);

    m_pCropScene->addItem(m_pPixmapItem);
//    auto pixmapSize = m_pPixmapItem->boundingRect();
//    m_cropArea->setSceneRect(0, 0, 1200, 1000);


    connect(ui->closeButton, &QPushButton::clicked, this, &CropEditorDialog::hide);
    connect(ui->loadButton, &QPushButton::clicked, this,
            [=]() { emit signalLoadPixmap(); });
}

CropEditorDialog::~CropEditorDialog()
{
    delete ui;
}

void CropEditorDialog::setPixmap(const QPixmap & a_pixmap)
{
    m_pPixmapItem->setPixmap(a_pixmap);

    QVector<QRgb> invertedColorTable(256);
//    m_baseImage = a_pixmap.toImage();
    m_pCropScene->update();
//    m_pPixmapItem->update();
//    qDebug() << m_pPixmapItem->boundingRect().width() << m_pPixmapItem->boundingRect().height();
    //    qDebug() << m_pixmapItem->BoundingRectShape;
}

void CropEditorDialog::hideEvent(QHideEvent *a_pEvent)
{
    emit signalDialogHidden();
    QDialog::hideEvent(a_pEvent);
}
