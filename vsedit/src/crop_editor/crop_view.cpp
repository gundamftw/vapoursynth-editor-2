#include "crop_view.h"
#include "crop_frame.h"

#include <QGraphicsPixmapItem>
#include <QMouseEvent>
#include <QDebug>

CropView::CropView(QWidget *parent): QGraphicsView(parent),
    m_leftMouseButtonPressed(false)
{
    setGeometry(0, 0, 900, 600);

//    m_crop_area = new QGraphicsScene(this);
//    this->setScene(m_crop_area);
//    m_frame = new CropFrame();
//    m_crop_area->addItem(m_frame);

}

void CropView::mousePressEvent(QMouseEvent *a_pEvent)
{

}

void CropView::mouseMoveEvent(QMouseEvent *a_pEvent)
{
}

void CropView::mouseReleaseEvent(QMouseEvent *a_pEvent)
{
    m_leftMouseButtonPressed = false;
}
