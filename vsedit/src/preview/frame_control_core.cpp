#include "frame_control_core.h"
#include "../../../common-src/vapoursynth/vapoursynth_script_processor.h"
#include "math.h"
#include "../../../common-src/helpers.h"

#include <vapoursynth/VapourSynth4.h>
#include <vapoursynth/VSHelper4.h>

#include <QTimer>

FrameControlCore::FrameControlCore(SettingsManager * a_pSettingsManager,
                                 VSScriptLibrary * a_pVSScriptLibrary, QWidget * a_pParent) :
    VSScriptFrameHandler(a_pSettingsManager, a_pVSScriptLibrary, a_pParent)
  , m_frameExpected(0)
  , m_frameShown(-1)
  , m_lastFrameRequestedForPlay(-1)
  , m_cpFrame(nullptr)
  , m_cpPreviewFrame(nullptr)
  , m_cpScopeFrame(nullptr)
  , m_scopeNum(0)
  , m_playing(false)
  , m_processingPlayQueue(false)
  , m_secondsBetweenFrames(0)
  , m_pPlayTimer(nullptr)
//  , m_alwaysKeepCurrentFrame(DEFAULT_ALWAYS_KEEP_CURRENT_FRAME)
{
    m_pPlayTimer = new QTimer(this);
    m_pPlayTimer->setTimerType(Qt::PreciseTimer);
    m_pPlayTimer->setSingleShot(true);

    connect(m_pPlayTimer, SIGNAL(timeout()),
            this, SLOT(slotProcessPlayQueue()));
}

FrameControlCore::~FrameControlCore()
{
    stopAndCleanUp();
}

void FrameControlCore::setScriptName(const QString &a_scriptName)
{
    VSScriptFrameHandler::setScriptName(a_scriptName);
}

const VSVideoInfo * FrameControlCore::vsVideoInfo()
{
    return m_cpVideoInfo;
}

void FrameControlCore::setCurrentFrame(const VSFrame *a_cpOutputFrame, const VSFrame *a_cpPreviewFrame,
    const VSFrame *a_cpScopeFrame)
{
    Q_ASSERT(m_cpVSAPI);
    m_cpVSAPI->freeFrame(m_cpFrame); // free frame from last reference frame
    m_cpFrame = a_cpOutputFrame;

    if (m_cpPreviewFrame)
    {
        m_cpVSAPI->freeFrame(m_cpPreviewFrame);
        m_cpPreviewFrame = nullptr;
    }

    if (a_cpScopeFrame != nullptr) {
        if (m_scopeNum > 0) {
            if (m_cpScopeFrame) {
                m_cpVSAPI->freeFrame(m_cpScopeFrame);
                m_cpScopeFrame = nullptr;
            }

            m_scopePixmap = pixmapFromPackedRGB24(a_cpScopeFrame).scaled(480, 320);
            m_cpScopeFrame = a_cpScopeFrame;
            emit signalSendScopePixmap(m_scopePixmap);
        }
    }

    m_framePixmap = pixmapFromPackedRGB24(a_cpPreviewFrame);
    m_cpPreviewFrame = a_cpPreviewFrame;

    QString framePropsString = m_pVapourSynthScriptProcessor->framePropsString(m_cpFrame);

    emit signalUpdateFramePropsString(framePropsString); // send frame properties string
    emit signalSetCurrentFrame(m_framePixmap); // send framePixmap to preview area

}

const VSFrame *FrameControlCore::frame()
{
    return m_cpFrame;
}

int FrameControlCore::currentFrame()
{
    return m_frameShown;
}

QImage FrameControlCore::currentFrameImage()
{
    return m_frameImage;
}

QPixmap FrameControlCore::currentWaveformPixmap()
{
    return m_scopePixmap;
}

bool FrameControlCore::previewScript(const QString &a_script, const QString &a_scriptName)
{
    stopAndCleanUp();

    bool initialized = initialize(a_script, a_scriptName);
    if(!initialized) {
        return false;
    }

    int lastFrameNumber = m_cpVideoInfo->numFrames - 1;

    // emit signal to setup timeline and range of spinbox
    emit signalSetTimeLineAndIndicator(lastFrameNumber, m_cpVideoInfo->fpsNum, m_cpVideoInfo->fpsDen);

    if(m_frameExpected > lastFrameNumber)
        m_frameExpected = lastFrameNumber;

    setScriptName(a_scriptName);

    slotShowFrame(m_frameExpected);
    return true;
}

bool FrameControlCore::updatePreviewFilters(PreviewFilters & a_previewFilters)
{
    m_pVapourSynthScriptProcessor->slotApplyPreviewFilters(a_previewFilters);

    // request same frame again
    if (!m_playing)
        requestFrame(m_frameShown);

    return true; // test
}

void FrameControlCore::showFrameFromTimeLine(int a_frameNumber)
{
    slotShowFrame(a_frameNumber);
}

void FrameControlCore::showFrameFromFrameIndicator(int a_frameNumber)
{

}

bool FrameControlCore::isPlaying()
{
    return m_playing;
}

void FrameControlCore::stopAndCleanUp()
{
    slotPlay(false);

    m_frameShown = -1;

    if(m_cpFrame)
    {
        Q_ASSERT(m_cpVSAPI);
        m_cpVSAPI->freeFrame(m_cpFrame);
        m_cpFrame = nullptr;
    }

    if(m_cpPreviewFrame)
    {
        m_cpVSAPI->freeFrame(m_cpPreviewFrame);
        m_cpPreviewFrame = nullptr;
    }

    if(m_cpScopeFrame)
    {
        m_cpVSAPI->freeFrame(m_cpScopeFrame);
        m_cpScopeFrame = nullptr;
    }

    VSScriptFrameHandler::stopAndCleanUp();
}

bool FrameControlCore::requestFrame(int a_frameNumber)
{
    if(!m_pVapourSynthScriptProcessor->isInitialized())
        return false;

    if((m_frameShown != -1) && (m_frameShown != m_frameExpected))
        return false;

    m_pVapourSynthScriptProcessor->requestFrameAsync(a_frameNumber, 0, true, m_playing, m_scopeNum);
    return true;
}

QPixmap FrameControlCore::pixmapFromPackedRGB24(const VSFrame *a_cpFrame)
{
    if((!m_cpVSAPI) || (!a_cpFrame))
        return QPixmap();

    const VSVideoFormat * cpFormat = m_cpVSAPI->getVideoFrameFormat(a_cpFrame);
    Q_ASSERT(cpFormat);

    VSCore * core = getVSEditCore();
    bool isVideoGray32 = vsh::isSameVideoPresetFormat(pfGray32, cpFormat, core, m_cpVSAPI);

    if(!isVideoGray32)
    {
        char formatName[32];
        m_cpVSAPI->getVideoFormatName(cpFormat, formatName);

        QString errorString = tr("Error forming image from frame. "
            "Expected format packed RGB24(GRAY32). Instead got \'%1\'.")
            .arg(formatName);
        emit signalWriteLogMessage(mtCritical, errorString);
        return QPixmap();
    }

    auto pData = m_cpVSAPI->getReadPtr(a_cpFrame, 0);

    int width = m_cpVSAPI->getFrameWidth(a_cpFrame, 0);
    int height = m_cpVSAPI->getFrameHeight(a_cpFrame, 0);
    auto stride = m_cpVSAPI->getStride(a_cpFrame, 0);

    QImage frameImage(pData, width, height, stride, QImage::Format_RGB32);
    QPixmap framePixmap = QPixmap::fromImage(frameImage, Qt::NoFormatConversion);

    m_frameImage = frameImage;
    return framePixmap;
}

void FrameControlCore::slotDistributeFrame(int a_frameNumber, int a_outputIndex,
    const VSFrame *a_cpOutputFrame, const VSFrame *a_cpPreviewFrame,
    const VSFrame *a_cpScopeFrame)
{
    Q_ASSERT(m_cpVSAPI);
    const VSFrame * cpOutputFrame = nullptr;
    const VSFrame * cpPreviewFrame = nullptr;
    const VSFrame * cpScopeFrame = nullptr;

    if(!m_playing) {
        if(!a_cpOutputFrame)
            return;

        cpOutputFrame = m_cpVSAPI->addFrameRef(a_cpOutputFrame);
    }

    if(a_cpPreviewFrame) {
        cpPreviewFrame = m_cpVSAPI->addFrameRef(a_cpPreviewFrame);
    }

    if (m_scopeNum > 0 && a_cpScopeFrame) {
        cpScopeFrame = m_cpVSAPI->addFrameRef(a_cpScopeFrame);
    }

    if(m_playing)
    {
        Frame newFrame(a_frameNumber, a_outputIndex, nullptr, cpPreviewFrame, a_cpScopeFrame);
        m_framesCache.push_back(newFrame);
        slotProcessPlayQueue();
    }
    else
    {
        setCurrentFrame(cpOutputFrame, cpPreviewFrame, cpScopeFrame);
        m_frameShown = a_frameNumber;
        if(m_frameShown == m_frameExpected) {
//            m_ui.frameStatusLabel->setPixmap(m_readyPixmap);
        }
    }
}

void FrameControlCore::slotFrameRequestDiscarded(int a_frameNumber, int a_outputIndex, const QString &a_reason)
{
    (void)a_outputIndex;
    (void)a_reason;

    if(m_playing)
    {
        slotPlay(false);
    }
    else
    {
        if(a_frameNumber != m_frameExpected)
            return;

        if(m_frameShown == -1)
        {
            if(m_frameExpected == 0)
            {
                // Nowhere to roll back
                emit signalRollBackFrame(0);
//                m_ui.frameStatusLabel->setPixmap(m_errorPixmap);
            }
            else
                slotShowFrame(0);
            return;
        }

        m_frameExpected = m_frameShown;
        emit signalRollBackFrame(m_frameShown);
//        m_ui.frameStatusLabel->setPixmap(m_readyPixmap);
    }
}

void FrameControlCore::slotProcessPlayQueue()
{
    if(!m_playing)
        return;

    if (m_frameShown == m_cpVideoInfo->numFrames - 1) {
        slotPlay(false); // stop playing at the end
        return;
    }

    if(m_processingPlayQueue)
        return;

    m_processingPlayQueue = true;

    int nextFrame = (m_frameShown + 1) % m_cpVideoInfo->numFrames;
    Frame referenceFrame(nextFrame, 0, nullptr);

    while(!m_framesCache.empty())
    {
        //  find next frame in frame cache
        auto it = std::find(m_framesCache.begin(), m_framesCache.end(),
                            referenceFrame);

        if(it == m_framesCache.end())
            break;

        hr_time_point now = hr_clock::now();
        double passed = duration_to_double(now - m_lastFrameShowTime);
        double secondsToNextFrame = m_secondsBetweenFrames - passed;

        if(secondsToNextFrame > 0)
        {
            int millisecondsToNextFrame = int(std::ceil(secondsToNextFrame * 1000));
            m_pPlayTimer->start(millisecondsToNextFrame);
            break;
        }

        setCurrentFrame(it->cpOutputFrame, it->cpPreviewFrame, it->cpWaveformFrame); // set pix and send to previewarea

        m_lastFrameShowTime = hr_clock::now();

        m_frameShown = nextFrame;
        m_frameExpected = m_frameShown;

        emit signalFrameChanged(m_frameExpected); // signal to change timeline slider position

        m_framesCache.erase(it);
        nextFrame = (m_frameShown + 1) % m_cpVideoInfo->numFrames;
        referenceFrame.number = nextFrame;
    }

    nextFrame = (m_lastFrameRequestedForPlay + 1) %
        m_cpVideoInfo->numFrames;

    /* request next frame to que */
    while(((m_framesInQueue + m_framesInProcess) < m_maxThreads) &&
          (m_framesCache.size() <= m_cachedFramesLimit))
    {
        m_pVapourSynthScriptProcessor->requestFrameAsync(nextFrame, 0, true, m_playing, m_scopeNum);
        m_lastFrameRequestedForPlay = nextFrame;
        nextFrame = (nextFrame + 1) % m_cpVideoInfo->numFrames;
    }

    m_processingPlayQueue = false;
}

bool FrameControlCore::slotPlay(bool a_play)
{
    if(m_playing == a_play)
        return true;

    m_playing = a_play;

    if(m_playing)
    {
        m_lastFrameRequestedForPlay = m_frameShown;
        slotProcessPlayQueue();
    }
    else
    {
        m_pVapourSynthScriptProcessor->flushFrameTicketsQueue();
        clearFramesCache();
    }

    return m_playing;
}

void FrameControlCore::slotSetPlaySpeed(double a_secondsPerFrames)
{
//    if (m_secondsBetweenFrames == a_secondsPerFrames) return;
    m_secondsBetweenFrames = a_secondsPerFrames;
}

void FrameControlCore::slotGotoFrame(int a_frameNumber)
{
    if ((a_frameNumber > m_cpVideoInfo->numFrames) || (a_frameNumber < 0))
        m_frameShown = 0;

    m_frameShown = a_frameNumber;
}

void FrameControlCore::slotChangeScope(int a_scopeNum)
{
    if (m_scopeNum != a_scopeNum)
        m_scopeNum = a_scopeNum;

    requestFrame(m_frameShown);
}

void FrameControlCore::slotShowFrame(int a_frameNumber)
{
    if(m_playing)
        return;

    if(m_frameShown == a_frameNumber)
        return;

    if ((a_frameNumber > m_cpVideoInfo->numFrames) || (a_frameNumber < 0))
        return;

    static bool requestingFrame = false;
    if(requestingFrame)
        return;
    requestingFrame = true;

    bool requested = requestFrame(a_frameNumber); // request to output frame
    if(requested)
    {
        m_frameExpected = a_frameNumber;
//		m_ui.frameStatusLabel->setPixmap(m_busyPixmap);
    }

    requestingFrame = false;
}

void FrameControlCore::slotJumpPlay(int a_frameNumber)
{
    if (!m_playing)
        return;

    m_frameShown = a_frameNumber;
    slotPlay(false);
    slotPlay(true);
}
