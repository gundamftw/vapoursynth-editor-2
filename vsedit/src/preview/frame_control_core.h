#ifndef FRAME_CONTROL_CORE_H
#define FRAME_CONTROL_CORE_H

#include "../../vsedit/src/vapoursynth/vs_script_frame_handler.h"
#include "../../../common-src/chrono.h"

#include <QObject>
#include <QWidget>


class FrameControlCore : public VSScriptFrameHandler
{
    Q_OBJECT
public:
    explicit FrameControlCore(SettingsManager * a_pSettingsManager,
                        VSScriptLibrary * a_pVSScriptLibrary, QWidget * a_pParent = nullptr);

    ~FrameControlCore() override;

    virtual void setScriptName(const QString & a_scriptName) override;

    const VSVideoInfo * vsVideoInfo();

    const VSFrame *frame();

    int currentFrame();

    QImage currentFrameImage();
    QPixmap currentWaveformPixmap();

    bool previewScript(const QString& a_script, const QString& a_scriptName);

    bool updatePreviewFilters(PreviewFilters & a_previewFilters);

    void showFrameFromTimeLine(int a_frameNumber);

    void showFrameFromFrameIndicator(int a_frameNumber);

    bool isPlaying();

protected:

    void stopAndCleanUp() override;

    bool requestFrame(int a_frameNumber);

    void setCurrentFrame(const VSFrame * a_cpOutputFrame, const VSFrame * a_cpPreviewFrame,
        const VSFrame * a_cpScopeFrame = nullptr);

    QPixmap pixmapFromPackedRGB24(const VSFrame * a_cpFrame);

    const VSFrame * m_cpFrame;
    const VSFrame * m_cpPreviewFrame;
    const VSFrame * m_cpScopeFrame;
    QImage m_frameImage;
    QPixmap m_framePixmap;
    QPixmap m_scopePixmap;

    int m_scopeNum;

    bool m_playing;
    bool m_processingPlayQueue;
    hr_time_point m_lastFrameShowTime;
    double m_secondsBetweenFrames;
    QTimer * m_pPlayTimer;

    int m_frameExpected;
    int m_frameShown;
    int m_lastFrameRequestedForPlay;


protected slots:

    virtual void slotDistributeFrame(int a_frameNumber, int a_outputIndex,
        const VSFrame * a_cpOutputFrame, const VSFrame * a_cpPreviewFrame,
        const VSFrame * a_cpScopeFrame = nullptr) override;

    virtual void slotFrameRequestDiscarded(int a_frameNumber,
        int a_outputIndex, const QString & a_reason) override;

    void slotProcessPlayQueue();

    void slotShowFrame(int a_frameNumber);

public slots:

    void slotJumpPlay(int a_frameNumber); // set frame when jump in timeline during play
    bool slotPlay(bool a_play);
    void slotSetPlaySpeed(double a_secondsPerFrames);
    void slotGotoFrame(int a_frameNumber);
    void slotChangeScope(int a_scopeNum);

signals:

    void signalSetTimeLineAndIndicator(int a_numFrame, int64_t a_fpsNum, int64_t a_fpsDen);

    void signalSetCurrentFrame(const QPixmap &a_framePixmap);

    void signalFrameChanged(int a_frame);

    void signalRollBackFrame(int a_frame);

    void signalUpdateFrameTimeIndicators(int a_frameIndex, const QTime &a_time);

    void signalUpdateFramePropsString(const QString & a_framePropsString);

    void signalSendScopePixmap(const QPixmap& a_scopePixmap);

};

#endif // FRAME_CONTROL_CORE_H
