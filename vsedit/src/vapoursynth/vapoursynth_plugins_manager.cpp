#include "vapoursynth_plugins_manager.h"

#include "../../../common-src/helpers.h"
#include "../../../common-src/settings/settings_manager.h"

#include <QDir>
#include <QLibrary>
#include <QFileInfoList>
#include <QSettings>
#include <QProcessEnvironment>
#include <QDirIterator>
#include <algorithm>

//==============================================================================

const char CORE_PLUGINS_FILEPATH[] = "core";

//==============================================================================

VapourSynthPluginsManager::VapourSynthPluginsManager(
	SettingsManager * a_pSettingsManager, QObject * a_pParent):
	QObject(a_pParent)
	, m_pluginsList()
    , m_pyScriptsList()
	, m_currentPluginPath()
	, m_pluginAlreadyLoaded(false)
	, m_pSettingsManager(a_pSettingsManager)
    , m_vsRepoPath()
{
	if(a_pParent)
	{
		connect(this, SIGNAL(signalWriteLogMessage(int, const QString &)),
		a_pParent, SLOT(slotWriteLogMessage(int, const QString &)));
	}

    loadVSRepoPath();
    getPyScripts();
	slotRefill();
}

// END OF VapourSynthPluginsManager::VapourSynthPluginsManager(
//		SettingsManager * a_pSettingsManager, QObject * a_pParent)
//==============================================================================

VapourSynthPluginsManager::~VapourSynthPluginsManager()
{
	slotClear();
}

// END OF VapourSynthPluginsManager::~VapourSynthPluginsManager()
//==============================================================================

void VapourSynthPluginsManager::getCorePlugins()
{
	QString libraryName("vapoursynth");
	QString libraryFullPath;
	QLibrary vsLibrary(libraryName);
	bool loaded = vsLibrary.load();

#ifdef Q_OS_WIN
	if(!loaded)
	{
		QSettings settings("HKEY_LOCAL_MACHINE\\SOFTWARE",
			QSettings::NativeFormat);
		libraryFullPath =
			settings.value("VapourSynth/VapourSynthDLL").toString();
		if(libraryFullPath.isEmpty())
		{
			libraryFullPath = settings.value(
				"Wow6432Node/VapourSynth/VapourSynthDLL").toString();
		}

		if(!libraryFullPath.isEmpty())
		{
			vsLibrary.setFileName(libraryFullPath);
			loaded = vsLibrary.load();
		}
	}

	if(!loaded)
	{
		QProcessEnvironment environment =
			QProcessEnvironment::systemEnvironment();
		QString basePath;

#ifdef Q_OS_WIN64
        basePath = environment.value("ProgramFiles");
        libraryFullPath = basePath + "\\VapourSynth\\core\\vapoursynth.dll";
#else
		basePath = environment.value("ProgramFiles");
		libraryFullPath = basePath + "\\VapourSynth\\core32\\vapoursynth.dll";
#endif // Q_OS_WIN64

		vsLibrary.setFileName(libraryFullPath);
		loaded = vsLibrary.load();
	}
#endif // Q_OS_WIN

	if(!loaded)
	{
		QStringList librarySearchPaths =
			m_pSettingsManager->getVapourSynthLibraryPaths();
		for(const QString & path : librarySearchPaths)
		{
			libraryFullPath = vsedit::resolvePathFromApplication(path) +
				QString("/") + libraryName;
			vsLibrary.setFileName(libraryFullPath);
			loaded = vsLibrary.load();
			if(loaded)
				break;
		}
	}

	if(!loaded)
	{
		emit signalWriteLogMessage(mtCritical, "VapourSynth plugins manager: "
			"Failed to load vapoursynth library!\n"
			"Please set up the library search paths in settings.");
		return;
	}

    VSGetVapourSynthAPI getVapourSynthAPI =
        VSGetVapourSynthAPI(vsLibrary.resolve("getVapourSynthAPI"));
	if(!getVapourSynthAPI)
	{ // Win32 fallback
		getVapourSynthAPI =
            VSGetVapourSynthAPI(vsLibrary.resolve("_getVapourSynthAPI@4"));
	}
	if(!getVapourSynthAPI)
	{
		emit signalWriteLogMessage(mtCritical, "VapourSynth plugins manager: "
			"Failed to get entry in vapoursynth library!");
		vsLibrary.unload();
		return;
	}

    // VSAPI v4
    const VSAPI * cpVSAPI = getVapourSynthAPI(VAPOURSYNTH_API_VERSION);
	if(!cpVSAPI)
	{
		emit signalWriteLogMessage(mtCritical, "VapourSynth plugins manager: "
			"Failed to get VapourSynth API!");
		vsLibrary.unload();
		return;
	}

    VSCore * pCore = cpVSAPI->createCore(ccfEnableGraphInspection);
	if(!pCore)
	{
		emit signalWriteLogMessage(mtCritical, "VapourSynth plugins manager: "
			"Failed to create VapourSynth core!");
		vsLibrary.unload();
		return;
	}

    // plugins used by vsedit2
    QStringList requiredPluginsLookUp = {"libp2p", "akarin", "hist"};

    VSPlugin * pPlugin = cpVSAPI->getNextPlugin(nullptr, pCore);
    while (pPlugin != nullptr) {
        // insert plugin properties
        m_pluginsList.emplace_back();
        VSData::Plugin & pluginData = m_pluginsList.back();
        pluginData.filepath = cpVSAPI->getPluginPath(pPlugin);
        pluginData.id = cpVSAPI->getPluginID(pPlugin);
        pluginData.pluginNamespace = cpVSAPI->getPluginNamespace(pPlugin);
        pluginData.name = cpVSAPI->getPluginName(pPlugin);

        // insert plugin functions
        VSPluginFunction * pPluginFunction = cpVSAPI->getNextPluginFunction(nullptr, pPlugin);
        while (pPluginFunction != nullptr) {

            const char * functionName = cpVSAPI->getPluginFunctionName(pPluginFunction);
            const char * functionArguments = cpVSAPI->getPluginFunctionArguments(pPluginFunction);

            VSData::Function function = parseFunctionSignature(functionName, functionArguments);
            pluginData.functions.push_back(function);

            pPluginFunction = cpVSAPI->getNextPluginFunction(pPluginFunction, pPlugin);
        }

        // checks if required plugins were loaded
        if (requiredPluginsLookUp.contains(pluginData.pluginNamespace))
            m_pSettingsManager->loadedRequiredPlugins.append(pluginData.pluginNamespace);

        pPlugin = cpVSAPI->getNextPlugin(pPlugin, pCore);
    }
    cpVSAPI->freeCore(pCore);
    vsLibrary.unload();
}

// END OF void VapourSynthPluginsManager::getCorePlugins()
//==============================================================================

void VapourSynthPluginsManager::getPyScripts()
{
    // get vsrepo path, then get the definition path
    QString definitionPath = definitionsPath();

    // parse the definition file, store package info for type "pyscript",
    QFile inFile(definitionPath);
    inFile.open(QIODevice::ReadOnly|QIODevice::Text);
    QString data = inFile.readAll();
    inFile.close();

    QJsonDocument d = QJsonDocument::fromJson(data.toUtf8());
    QJsonObject json = d.object();
    QJsonArray packagesArray = json["packages"].toArray();

    struct ModuleInfo {
        QString name;
        QString id;

        ModuleInfo(){}
        ModuleInfo(const QString &a_name, const QString &a_id):
            name(a_name), id(a_id)
        {}
    };

    // pack packages into a list for search
    QMap<QString, ModuleInfo> moduleNamesList;
    for (const auto package : packagesArray) {
        auto item = package.toObject();
        if (item["type"] == "PyScript") {
            auto moduleInfo = ModuleInfo();
            moduleInfo.id = item["identifier"].toString();
            moduleInfo.name = item["name"].toString();
            auto moduleName = item["modulename"].toString();
            moduleNamesList.insert(moduleName, moduleInfo);
        }
    }

    // use the list to look for py file in the script folder
    QString scriptsPath = pyScriptsPath();
    QString in;
    QDirIterator it(scriptsPath, {"*.py"}, QDir::Files);

    while (it.hasNext()) {
        QFile file(it.next());
        QString baseName = QFileInfo(file).baseName(); // filename without exten
        if (moduleNamesList.contains(baseName)) {
            file.open(QIODevice::ReadOnly);
            in = file.readAll();

            // store module info into PyScript data
            VSData::PyScript pyScriptData;
            pyScriptData.id = moduleNamesList[baseName].id;
            pyScriptData.moduleName = baseName;
            pyScriptData.name = moduleNamesList[baseName].name;

            // parse the module and store the functions into a big pyscript list
            QRegularExpression re("(?m)^def ([a-zA-Z]\\w*)\\(([^()]*)\\):");
            QRegularExpressionMatchIterator i = re.globalMatch(in);

            while (i.hasNext()) {
                QRegularExpressionMatch match = i.next();
                QString definition = match.captured(1); // function name
                QString args = match.captured(2); // args string

                VSData::Function function;
                function.name = definition;

                QStringList argumentsList = args.split(",");

                for(const QString& argumentString : argumentsList)
                {
                    QStringList argumentPairs = argumentString.split("=");

                    function.arguments.emplace_back();
                    VSData::FunctionArgument & argument = function.arguments.back();

                    // filter out "\" for some definitions
                    argument.name = argumentPairs[0].replace("\\", "").trimmed(); // for storing clip value

                    if (argumentPairs.count() > 1) {
                        argument.value = argumentPairs[1].trimmed();
                    }
                }

                pyScriptData.functions.push_back(function);
            }
            m_pyScriptsList.push_back(pyScriptData);
        }
    }
}

// END OF void VapourSynthPluginsManager::getPyScripts()
//==============================================================================

QString VapourSynthPluginsManager::vSRepoPath()
{
    return m_vsRepoPath;
}

// END OF void VapourSynthPluginsManager::VSRepoPath()
//==============================================================================

QString VapourSynthPluginsManager::pluginsPath()
{
    return getPathsByVSRepo("Binaries");
}

// END OF void VapourSynthPluginsManager::PluginsPath()
//==============================================================================

QString VapourSynthPluginsManager::pyScriptsPath()
{
    return getPathsByVSRepo("Scripts");
}

// END OF void VapourSynthPluginsManager::PyScriptsPath()
//==============================================================================

QString VapourSynthPluginsManager::definitionsPath()
{
    return getPathsByVSRepo("Definitions");
}

// END OF void VapourSynthPluginsManager::DefinitionsPath()
//==============================================================================

void VapourSynthPluginsManager::pollPaths(const QStringList & a_pluginsPaths)
{
	for(const QString & dirPath : a_pluginsPaths)
	{
		QString absolutePath = vsedit::resolvePathFromApplication(dirPath);
		QFileInfoList fileInfoList = QDir(absolutePath).entryInfoList();
		for(const QFileInfo & fileInfo : fileInfoList)
		{
			QString filePath = fileInfo.absoluteFilePath();
			QLibrary plugin(filePath);
			VSInitPlugin initVSPlugin =
                VSInitPlugin(plugin.resolve("VapourSynthPluginInit2"));

			if(!initVSPlugin)
			{ // Win32 fallback
				initVSPlugin =
                    VSInitPlugin(plugin.resolve("_VapourSynthPluginInit2@12"));
			}
			if(!initVSPlugin)
				continue;
			m_currentPluginPath = filePath;

            initVSPlugin(reinterpret_cast<VSPlugin *>(this), m_vspapi);

			plugin.unload();
			m_pluginAlreadyLoaded = false;
		}
	}
}

// END OF void VapourSynthPluginsManager::pollPaths(
//		const QStringList & a_pluginsPaths)
//==============================================================================

QStringList VapourSynthPluginsManager::functions() const
{
	QStringList functionsList;
	for(const VSData::Plugin & plugin : m_pluginsList)
	{
		QString functionNamespace = plugin.pluginNamespace + ".";
		for(const VSData::Function & function : plugin.functions)
			functionsList << functionNamespace + function.toString();
	}

	return functionsList;
}

// END OF QStringList VapourSynthPluginsManager::functions() const
//==============================================================================

VSPluginsList VapourSynthPluginsManager::pluginsList() const
{
    return m_pluginsList;
}

// END OF VSPluginsList VapourSynthPluginsManager::pluginsList() const
//==============================================================================

VSPyScriptsList VapourSynthPluginsManager::pyScriptsList() const
{
    return m_pyScriptsList;
}

// END OF VSPyScriptsList VapourSynthPluginsManager::pyScriptsList() const
//==============================================================================

VSData::Function VapourSynthPluginsManager::parseFunctionSignature(
	const QString & a_name, const QString & a_arguments)
{
	VSData::Function function;
	function.name = a_name;
    QStringList argumentsList = a_arguments.split(';', Qt::SkipEmptyParts);
	if(argumentsList.size() == 0)
		return function;

	// This is true for arguments lists returned by VSAPI.
	if(argumentsList[0] == a_name)
		argumentsList.removeFirst();

	for(const QString& argumentString : argumentsList)
	{
		QStringList argumentParts = argumentString.split(':');
		int partsNumber = argumentParts.size();
		if(partsNumber < 2)
			continue;

		function.arguments.emplace_back();
		VSData::FunctionArgument & argument = function.arguments.back();
		argument.name = argumentParts[0];
		argument.type = argumentParts[1];

		for(int i = 2; i < partsNumber; ++i)
		{
			if(argumentParts[i] == "opt")
				argument.optional = true;
			else if(argumentParts[i] == "empty")
				argument.empty = true;
		}
	}

	return function;
}

// END OF VSData::Function VapourSynthPluginsManager::parseFunctionSignature(
//		const QString & a_name, const QString & a_arguments)
//==============================================================================

void VapourSynthPluginsManager::slotClear()
{
	m_pluginsList.clear();
}

// END OF void VapourSynthPluginsManager::slotClear()
//==============================================================================

void VapourSynthPluginsManager::slotSort()
{
	std::stable_sort(m_pluginsList.begin(), m_pluginsList.end());
	for(VSData::Plugin & plugin : m_pluginsList)
		std::stable_sort(plugin.functions.begin(), plugin.functions.end());
}

// END OF void VapourSynthPluginsManager::slotSort()
//==============================================================================

void VapourSynthPluginsManager::slotRefill()
{
	slotClear();
	getCorePlugins();
	QStringList pluginsPaths = m_pSettingsManager->getVapourSynthPluginsPaths();
	pollPaths(pluginsPaths);
    slotSort();
}

// END OF void VapourSynthPluginsManager::slotRefill()
//==============================================================================

void VapourSynthPluginsManager::loadVSRepoPath()
{
#ifdef Q_OS_WIN
    QString vsRepoName("vsrepo.py");
    QString vsRepoPath;

    QSettings settings("HKEY_LOCAL_MACHINE\\SOFTWARE", QSettings::NativeFormat);
    vsRepoPath = settings.value("VapourSynth/VSRepoPY").toString();
    if(vsRepoPath.isEmpty())
    {
        vsRepoPath = settings.value(
                    "Wow6432Node/VapourSynth/VSRepoPY").toString();
    }

    if(vsRepoPath.isEmpty())
    {
        QProcessEnvironment environment = QProcessEnvironment::systemEnvironment();
        QString basePath;

        basePath = environment.value("ProgramFiles");
        vsRepoPath = basePath + "\\VapourSynth\\vsrepo\\vsrepo.py";
    }

    if(vsRepoPath.isEmpty())
    {
        QStringList librarySearchPaths = m_pSettingsManager->getVapourSynthLibraryPaths();
        for(const QString & path : librarySearchPaths)
        {
            m_vsRepoPath = vsedit::resolvePathFromApplication(path) +
                QString("/") + vsRepoName;
        }
    }

    if(vsRepoPath.isEmpty())
    {
        emit signalWriteLogMessage(mtCritical, "VapourSynth plugins manager: "
            "Failed to find vsrepo path.");
        return;
    }

    m_vsRepoPath = vsRepoPath;
#endif // Q_OS_WIN
}

QString VapourSynthPluginsManager::getPathsByVSRepo(const QString &a_key)
{
    /* vsrepo is Windows only */
#ifdef Q_OS_WIN
    QProcess * process = new QProcess(this);
    QStringList args;
    args << m_vsRepoPath << "paths";

    process->start("py", args);
    process->waitForFinished();
    process->setReadChannel(QProcess::StandardOutput);

    QString resultPath;
    while (process->canReadLine()) {
        QString line = process->readLine().trimmed();
        if (line.contains(a_key, Qt::CaseSensitive)) {
            resultPath= line.split(" ")[1]; // e.g. "Binaries: C:\VS\plugins"
            break;
        }
    }
    process->close();
    return resultPath;
#else
    return QString("");
#endif // Q_OS_WIN
}

// END OF void VapourSynthPluginsManager::loadVSRepoPath()
//==============================================================================
