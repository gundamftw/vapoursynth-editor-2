#ifndef VS_SCRIPT_FRAME_HANDLER_H_INCLUDED
#define VS_SCRIPT_FRAME_HANDLER_H_INCLUDED

#include "../../../common-src/vapoursynth/vs_script_processor_structures.h"
#include "../script_status_bar_widget/script_status_bar_widget.h"

#include <QDialog>
#include <QPixmap>
#include <list>

class QCloseEvent;
class QStatusBar;
class QLabel;
class SettingsManager;
class VSScriptLibrary;
class VapourSynthScriptProcessor;
struct VSAPI;

class VSScriptFrameHandler : public QDialog
{
	Q_OBJECT

public:

        VSScriptFrameHandler(SettingsManager * a_pSettingsManager,
                VSScriptLibrary * a_pVSScriptLibrary, QWidget * a_pParent = nullptr,
                Qt::WindowFlags a_flags =
        Qt::WindowFlags()
                | Qt::Window
                | Qt::CustomizeWindowHint
                | Qt::WindowMinimizeButtonHint
                | Qt::WindowMaximizeButtonHint
                | Qt::WindowCloseButtonHint);

    virtual ~VSScriptFrameHandler() override;

        virtual bool initialize(const QString & a_script,
                                const QString & a_scriptName);

        virtual bool busy() const;

        virtual const QString & script() const;

        virtual const QString & scriptName() const;

        virtual void setScriptName(const QString & a_scriptName);

    QString videoInfoString();

protected slots:

	virtual void slotWriteLogMessage(int a_messageType,
		const QString & a_message);

	virtual void slotFrameQueueStateChanged(size_t a_inQueue,
		size_t a_inProcess, size_t a_maxThreads);

	virtual void slotScriptProcessorFinalized();

        virtual void slotDistributeFrame(int a_frameNumber, int a_outputIndex,
        const VSFrame * a_cpOutputFrame, const VSFrame * a_cpPreviewFrame,
        const VSFrame * a_cpScopeFrame) = 0;

	virtual void slotFrameRequestDiscarded(int a_frameNumber,
		int a_outputIndex, const QString & a_reason) = 0;

signals:

    void signalFrameQueueStateChanged(size_t a_framesInQueue, size_t a_frameInProcess, size_t a_maxThreads);

    void signalWriteLogMessage(int a_messageType, const QString & a_message);

protected:

    virtual void closeEvent(QCloseEvent * a_pEvent) override;

    virtual void stopAndCleanUp();

    virtual void clearFramesCache();

	/// Adds status bar to the dialog.
	/// Relies on dialog having a layout.
	/// Call in derived class after GUI is created.
	virtual void createStatusBar();

    VSCore * getVSEditCore();

	SettingsManager * m_pSettingsManager;

	VSScriptLibrary * m_pVSScriptLibrary;

	VapourSynthScriptProcessor * m_pVapourSynthScriptProcessor;

	const VSAPI * m_cpVSAPI;

	const VSVideoInfo * m_cpVideoInfo;

	size_t m_framesInQueue;
	size_t m_framesInProcess;
	size_t m_maxThreads;

	bool m_wantToFinalize;
	bool m_wantToClose;

	QStatusBar * m_pStatusBar;
	ScriptStatusBarWidget * m_pStatusBarWidget;

	QPixmap m_readyPixmap;
	QPixmap m_busyPixmap;
	QPixmap m_errorPixmap;

	std::list<Frame> m_framesCache;
	size_t m_cachedFramesLimit;
};

#endif // VS_SCRIPT_FRAME_HANDLER_H_INCLUDED
