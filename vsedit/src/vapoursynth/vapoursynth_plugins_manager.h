#ifndef VAPOURSYNTHPLUGINSMANAGER_H
#define VAPOURSYNTHPLUGINSMANAGER_H

#include "vs_plugin_data.h"

#include <vapoursynth/VapourSynth4.h>

#include <QObject>
#include <QStringList>

class SettingsManager;

class VapourSynthPluginsManager : public QObject
{
	Q_OBJECT

public:

	VapourSynthPluginsManager(SettingsManager * a_pSettingsManager,
        QObject * a_pParent = nullptr);

    virtual ~VapourSynthPluginsManager() override;

    void getCorePlugins();

    void getPyScripts();

    QString vSRepoPath();

    QString pluginsPath();

    QString pyScriptsPath();

    QString definitionsPath();

	void pollPaths(const QStringList & a_pluginsPaths);

	QStringList functions() const;

	VSPluginsList pluginsList() const;

    VSPyScriptsList pyScriptsList() const;

	static VSData::Function parseFunctionSignature(const QString & a_name,
		const QString & a_arguments);

public slots:

	void slotClear();

	void slotSort();

	void slotRefill();

signals:

	void signalWriteLogMessage(int a_messageType,
		const QString & a_message);

private:

    void loadVSRepoPath();

    QString getPathsByVSRepo(const QString &a_key);

	VSPluginsList m_pluginsList;

    VSPyScriptsList m_pyScriptsList;

	QString m_currentPluginPath;

	bool m_pluginAlreadyLoaded;

	SettingsManager * m_pSettingsManager;

    QString m_vsRepoPath;

    const VSPLUGINAPI * m_vspapi;
};

#endif // VAPOURSYNTHPLUGINSMANAGER_H
