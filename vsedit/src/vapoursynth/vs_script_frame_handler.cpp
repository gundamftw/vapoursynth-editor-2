#include "vs_script_frame_handler.h"

#include "../../../common-src/helpers.h"
#include "../../../common-src/settings/settings_manager.h"
#include "../../../common-src/vapoursynth/vapoursynth_script_processor.h"
#include "../../../common-src/vapoursynth/vs_script_library.h"

#include <vapoursynth/VapourSynth4.h>

#include <QCloseEvent>
#include <QStatusBar>
#include <QLabel>
#include <QLayout>

//==============================================================================

VSScriptFrameHandler::VSScriptFrameHandler(
	SettingsManager * a_pSettingsManager, VSScriptLibrary * a_pVSScriptLibrary,
	QWidget * a_pParent, Qt::WindowFlags a_flags):
	  QDialog(a_pParent, a_flags)
	, m_pSettingsManager(a_pSettingsManager)
	, m_pVSScriptLibrary(a_pVSScriptLibrary)
	, m_pVapourSynthScriptProcessor(nullptr)
	, m_cpVSAPI(nullptr)
	, m_cpVideoInfo(nullptr)
	, m_framesInQueue(0)
	, m_framesInProcess(0)
	, m_maxThreads(0)
	, m_wantToFinalize(false)
	, m_wantToClose(false)
	, m_pStatusBar(nullptr)
	, m_pStatusBarWidget(nullptr)
	, m_readyPixmap(":tick.png")
	, m_busyPixmap(":busy.png")
	, m_errorPixmap(":cross.png")
    , m_cachedFramesLimit(1)
{
	Q_ASSERT(m_pSettingsManager);
	Q_ASSERT(m_pVSScriptLibrary);

//    connect(m_pVSScriptLibrary,
//        SIGNAL(signalWriteLogMessage(int, const QString &)),
//        this, SLOT(slotWriteLogMessage(int, const QString &)));

	m_pVapourSynthScriptProcessor = new VapourSynthScriptProcessor(
		m_pSettingsManager, m_pVSScriptLibrary, this);

    m_pStatusBarWidget = new ScriptStatusBarWidget(); // deprecated, widget is now a standalone

	connect(m_pVapourSynthScriptProcessor,
		SIGNAL(signalWriteLogMessage(int, const QString &)),
		this, SLOT(slotWriteLogMessage(int, const QString &)));
	connect(m_pVapourSynthScriptProcessor,
		SIGNAL(signalFrameQueueStateChanged(size_t, size_t, size_t)),
		this, SLOT(slotFrameQueueStateChanged(size_t, size_t, size_t)));
	connect(m_pVapourSynthScriptProcessor,
        SIGNAL(signalDistributeFrame(int, int, const VSFrame *, const VSFrame *, const VSFrame*)),
        this, SLOT(slotDistributeFrame(int, int, const VSFrame *, const VSFrame *, const VSFrame*)));
	connect(m_pVapourSynthScriptProcessor,
		SIGNAL(signalFrameRequestDiscarded(int, int, const QString &)),
		this, SLOT(slotFrameRequestDiscarded(int, int, const QString &)));
}

// END OF VSScriptProcessorDialog::VSScriptProcessorDialog(
//		SettingsManager * a_pSettingsManager,
//		VSScriptLibrary * a_pVSScriptLibrary,
//		QWidget * a_pParent, Qt::WindowFlags a_flags)
//==============================================================================

VSScriptFrameHandler::~VSScriptFrameHandler()
{
    stopAndCleanUp();
    m_pVapourSynthScriptProcessor->finalize();
}

// END OF VSScriptProcessorDialog::~VSScriptProcessorDialog()
//==============================================================================

bool VSScriptFrameHandler::initialize(const QString & a_script,
	const QString & a_scriptName)
{
	Q_ASSERT(m_pVapourSynthScriptProcessor);

    if(!m_cpVSAPI)
        m_cpVSAPI = m_pVSScriptLibrary->getVSAPI();

    if(!m_cpVSAPI)
		return false;

	if(m_pVapourSynthScriptProcessor->isInitialized())
	{
        stopAndCleanUp();
		bool finalized = m_pVapourSynthScriptProcessor->finalize();
		if(!finalized)
		{
			m_wantToFinalize = true;
			return false;
		}
	}

	bool initialized = m_pVapourSynthScriptProcessor->initialize(a_script,
		a_scriptName);

	if(!initialized)
	{
        m_pVapourSynthScriptProcessor->finalize();
		return false;
	}

    m_cpVideoInfo = m_pVapourSynthScriptProcessor->getVideoInfo();
	Q_ASSERT(m_cpVideoInfo);

//    m_pStatusBarWidget->setVideoInfo(m_cpVideoInfo);

	return true;
}

// END OF bool VSScriptProcessorDialog::initialize(const QString & a_script,
//		const QString & a_scriptName)
//==============================================================================

bool VSScriptFrameHandler::busy() const
{
	return ((m_framesInProcess + m_framesInQueue) != 0);
}

// END OF bool VSScriptProcessorDialog::busy()
//==============================================================================

const QString & VSScriptFrameHandler::script() const
{
	return m_pVapourSynthScriptProcessor->script();
}

// END OF const QString & VSScriptProcessorDialog::script() const
//==============================================================================

const QString & VSScriptFrameHandler::scriptName() const
{
	return m_pVapourSynthScriptProcessor->scriptName();
}

// END OF const QString & VSScriptProcessorDialog::scriptName() const
//==============================================================================

void VSScriptFrameHandler::setScriptName(const QString & a_scriptName)
{
	m_pVapourSynthScriptProcessor->setScriptName(a_scriptName);
}

// END OF void VSScriptProcessorDialog::setScriptName(
//		const QString & a_scriptName)
//==============================================================================

void VSScriptFrameHandler::slotWriteLogMessage(int a_messageType,
	const QString & a_message)
{
	emit signalWriteLogMessage(a_messageType, a_message);
}

// END OF void VSScriptProcessorDialog::slotWriteLogMessage(int a_messageType,
//		const QString & a_message)
//==============================================================================

void VSScriptFrameHandler::slotFrameQueueStateChanged(size_t a_inQueue,
	size_t a_inProcess, size_t a_maxThreads)
{
	m_framesInQueue = a_inQueue;
	m_framesInProcess = a_inProcess;
	m_maxThreads = a_maxThreads;

    emit signalFrameQueueStateChanged(m_framesInQueue, m_framesInProcess, m_maxThreads);
//	m_pStatusBarWidget->setQueueState(m_framesInQueue, m_framesInProcess,
//		m_maxThreads);
}

// END OF void VSScriptProcessorDialog::slotFrameQueueStateChanged(
//		size_t a_inQueue, size_t a_inProcess, size_t a_maxThreads)
//==============================================================================

void VSScriptFrameHandler::slotScriptProcessorFinalized()
{
	m_wantToFinalize = false;
	if(m_wantToClose)
	{
		m_wantToClose = false;
		close();
	}
}

// END OF void VSScriptProcessorDialog::slotScriptProcessofFinalized()
//==============================================================================


void VSScriptFrameHandler::closeEvent(QCloseEvent * a_pEvent)
{
	if(m_wantToClose)
		return;

	m_wantToClose = true;
	stopAndCleanUp();

	bool finalized = m_pVapourSynthScriptProcessor->finalize();
	if(!finalized)
	{
		m_wantToFinalize = true;
        emit signalWriteLogMessage(mtWarning, tr("Script processor "
			"is busy. Dialog will close when it is finalized."));
		a_pEvent->ignore();
		return;
	}

	QDialog::closeEvent(a_pEvent);
	m_wantToClose = false;
}

// END OF void VSScriptProcessorDialog::closeEvent(QCloseEvent * a_pEvent)
//==============================================================================

void VSScriptFrameHandler::stopAndCleanUp()
{
	clearFramesCache();
	m_cpVideoInfo = nullptr;
}

// END OF void VSScriptProcessorDialog::stopAndCleanUp()
//==============================================================================

void VSScriptFrameHandler::clearFramesCache()
{
	if(m_framesCache.empty())
		return;

	Q_ASSERT(m_cpVSAPI);
	for(Frame & frame : m_framesCache)
	{
        m_cpVSAPI->freeFrame(frame.cpOutputFrame);
        m_cpVSAPI->freeFrame(frame.cpPreviewFrame);
        m_cpVSAPI->freeFrame(frame.cpWaveformFrame);
	}
	m_framesCache.clear();
}

// END OF void VSScriptProcessorDialog::stopAndCleanUp()
//==============================================================================

void VSScriptFrameHandler::createStatusBar()
{
    QLayout * pLayout = layout();
	Q_ASSERT(pLayout);
	if(!pLayout)
		return;

	m_pStatusBar = new QStatusBar(this);
    m_pStatusBar->setSizeGripEnabled(false);
	pLayout->addWidget(m_pStatusBar);

    m_pStatusBar->addPermanentWidget(m_pStatusBarWidget, 1);
}

VSCore *VSScriptFrameHandler::getVSEditCore()
{
    return m_pVSScriptLibrary->getVSEditCore();
}

// END OF void VSScriptProcessorDialog::createStatusBar()
//==============================================================================

QString VSScriptFrameHandler::videoInfoString()
{
    if (!m_cpVideoInfo) {
        return "";
    }

    double fps = 0.0;
    double time = 0.0;
    if(m_cpVideoInfo->fpsDen != 0)
    {
        fps = double(m_cpVideoInfo->fpsNum) / double(m_cpVideoInfo->fpsDen);
        time = double(m_cpVideoInfo->numFrames) *
                double(m_cpVideoInfo->fpsDen) / double(m_cpVideoInfo->fpsNum);
    }

    QString infoString = QString("Frames: %frames% | Time: %time% | Size: "
                "%width%x%height% | FPS: %fpsnum%/%fpsden% = %fps% | Format: %format%");
    infoString.replace("%frames%", QString::number(m_cpVideoInfo->numFrames));
    infoString.replace("%time%", vsedit::timeToString(time, true));
    infoString.replace("%width%", QString::number(m_cpVideoInfo->width));
    infoString.replace("%height%", QString::number(m_cpVideoInfo->height));
    infoString.replace("%fpsnum%", QString::number(m_cpVideoInfo->fpsNum));
    infoString.replace("%fpsden%", QString::number(m_cpVideoInfo->fpsDen));
    infoString.replace("%fps%", QString::number(fps));

    char formatName[32];
    m_cpVSAPI->getVideoFormatName(&m_cpVideoInfo->format, formatName);
    infoString.replace("%format%", formatName);

    return infoString;
}
