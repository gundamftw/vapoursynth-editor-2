#ifndef PREVIEW_FILTERS_DIALOG_H
#define PREVIEW_FILTERS_DIALOG_H

#include "../../common-src/vapoursynth/vs_script_processor_structures.h"

#include <QDialog>
#include <QButtonGroup>

class SettingsManager;

namespace Ui {
class PreviewFiltersDialog;
}

class PreviewFiltersDialog : public QDialog
{
    Q_OBJECT

public:
    explicit PreviewFiltersDialog(SettingsManager * a_pSettingsManager,
                                  QWidget *parent = nullptr);
    ~PreviewFiltersDialog() override;

private:
    Ui::PreviewFiltersDialog *ui;

    SettingsManager * m_pSettingsManager;

    QButtonGroup * m_pPlanesButtonGroup;

    PreviewFilters m_previewFilters;

    int m_selectedPlaneId;
    int m_selectedMatrixCoefficient;

    void setFilterButtons();

protected:

    void moveEvent(QMoveEvent * a_pEvent) override;
    void hideEvent(QHideEvent * a_pEvent) override;

    void saveGeometryDelayed();
    void setWindowGeometry();

    QTimer * m_pGeometrySaveTimer;
    QByteArray m_windowGeometry;

signals:

    void signalChannelsButtonClicked(int);
    void signalDialogHidden();
    void signalPreviewFiltersChanged(PreviewFilters & a_previewFilters);

public slots:

    void slotUpdateDisplay(PreviewFilters &);

private slots:

    void slotUpdatePlanes(int);

    void slotUpdateYuvMatrixCoefficients(int);

protected slots:

    void slotSaveGeometry();
};

#endif // PREVIEW_FILTERS_DIALOG_H
