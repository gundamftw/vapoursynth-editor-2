#include "preview_filters_dialog.h"
#include "ui_preview_filters_dialog.h"
#include "../../common-src/settings/settings_manager.h"

#include <QTimer>
#include <QDebug>

PreviewFiltersDialog::PreviewFiltersDialog(SettingsManager * a_pSettingsManager,
                                           QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PreviewFiltersDialog),
    m_pSettingsManager(a_pSettingsManager),
    m_previewFilters(),
    m_selectedPlaneId(-1),
    m_selectedMatrixCoefficient(int(YuvMatrixCoefficients::m709))
{
    ui->setupUi(this);

    setWindowGeometry();

    m_pPlanesButtonGroup = new QButtonGroup(this);
    m_pPlanesButtonGroup->addButton(ui->allPlanesButton, -1);
    m_pPlanesButtonGroup->addButton(ui->yPlaneButton, int(YUVPlane::Y));
    m_pPlanesButtonGroup->addButton(ui->uPlaneButton, int(YUVPlane::U));
    m_pPlanesButtonGroup->addButton(ui->vPlaneButton, int(YUVPlane::V));

    connect(m_pPlanesButtonGroup, &QButtonGroup::idClicked,
            this, &PreviewFiltersDialog::slotUpdatePlanes);

    ui->yuvMatrixCoefficientsComboBox->addItem(
                tr("709"), int(YuvMatrixCoefficients::m709));
    ui->yuvMatrixCoefficientsComboBox->addItem(
                tr("470BG"), int(YuvMatrixCoefficients::m470BG));
    ui->yuvMatrixCoefficientsComboBox->addItem(
                tr("170M"), int(YuvMatrixCoefficients::m170M));
    ui->yuvMatrixCoefficientsComboBox->addItem(
                tr("2020 NCL"), int(YuvMatrixCoefficients::m2020_NCL));
    ui->yuvMatrixCoefficientsComboBox->addItem(
                tr("2020 CL"), int(YuvMatrixCoefficients::m2020_CL));

    connect(ui->yuvMatrixCoefficientsComboBox, QOverload<int>::of(&QComboBox::currentIndexChanged),
            this, &PreviewFiltersDialog::slotUpdateYuvMatrixCoefficients);

}

PreviewFiltersDialog::~PreviewFiltersDialog()
{
    delete ui;
}

void PreviewFiltersDialog::setFilterButtons()
{
    /* slot for setting button only, don't emit signal */
    QAbstractButton * planeButton = m_pPlanesButtonGroup->button(int(m_previewFilters.plane));

    if (planeButton != nullptr) {
        blockSignals(true);
        planeButton->setChecked(true);
        blockSignals(false);
    }
}

void PreviewFiltersDialog::moveEvent(QMoveEvent *a_pEvent)
{
    QDialog::moveEvent(a_pEvent);
    saveGeometryDelayed();
}

void PreviewFiltersDialog::hideEvent(QHideEvent *a_pEvent)
{
    emit signalDialogHidden();
    QDialog::hideEvent(a_pEvent);
    saveGeometryDelayed();
}

void PreviewFiltersDialog::saveGeometryDelayed()
{
    QApplication::processEvents();
    if(!isMaximized())
    {
        m_windowGeometry = saveGeometry();
        m_pGeometrySaveTimer->start();
    }
}

void PreviewFiltersDialog::setWindowGeometry()
{
    m_pGeometrySaveTimer = new QTimer(this);
    m_pGeometrySaveTimer->setInterval(DEFAULT_WINDOW_GEOMETRY_SAVE_DELAY);
    connect(m_pGeometrySaveTimer, &QTimer::timeout,
            this, &PreviewFiltersDialog::slotSaveGeometry);

    m_windowGeometry = m_pSettingsManager->getPreviewFiltersDialogGeometry();
    if(!m_windowGeometry.isEmpty())
        restoreGeometry(m_windowGeometry);
}

void PreviewFiltersDialog::slotUpdateDisplay(PreviewFilters & a_previewFilters)
{
    if (a_previewFilters != m_previewFilters) {
        m_previewFilters = a_previewFilters;
        setFilterButtons();
    }
}

void PreviewFiltersDialog::slotUpdatePlanes(int a_id)
{
    if (a_id != m_selectedPlaneId) {
        if (a_id > -1) {
            m_previewFilters.planeActive = true;
            m_previewFilters.plane = YUVPlane(a_id);
        } else {
            m_previewFilters.planeActive = false; //
        }

        m_previewFilters.updateFilters = true;
        m_selectedPlaneId = a_id;

        // check if previewfilter is turn off
        if (m_previewFilters == PreviewFilters()) {
            m_previewFilters.updateFilters = false;
        }
        emit signalPreviewFiltersChanged(m_previewFilters);
    }
}

void PreviewFiltersDialog::slotUpdateYuvMatrixCoefficients(int a_id)
{
    m_previewFilters.updateFilters = true;
    int m_selectedMatrixCoefficient = ui->yuvMatrixCoefficientsComboBox->currentData().toInt();
    m_previewFilters.colorMatrix = YuvMatrixCoefficients(m_selectedMatrixCoefficient);

    emit signalPreviewFiltersChanged(m_previewFilters);
}

void PreviewFiltersDialog::slotSaveGeometry()
{
    m_pGeometrySaveTimer->stop();
    m_pSettingsManager->setPreviewFiltersDialogGeometry(m_windowGeometry);
}
