#ifndef VS_SCRIPT_LIBRARY_H_INCLUDED
#define VS_SCRIPT_LIBRARY_H_INCLUDED

#include <vapoursynth/VSScript4.h>

#include <QObject>
#include <QLibrary>

class SettingsManagerCore;

//==============================================================================

typedef const VSSCRIPTAPI * (VS_CC *FNP_vssGetVSScriptAPI)(int a_version);

//==============================================================================

class VSScriptLibrary : public QObject
{
	Q_OBJECT

public:

	VSScriptLibrary(SettingsManagerCore * a_pSettingsManager,
		QObject * a_pParent = nullptr);

    virtual ~VSScriptLibrary() override;

	bool initialize();

	bool finalize();

	bool isInitialized() const;

    const VSAPI * getVSAPI();

    const VSSCRIPTAPI * getVSScriptAPI();

    VSCore * getVSEditCore();

signals:

	void signalWriteLogMessage(int a_messageType, const QString & a_message);

private:

	bool initLibrary();

	void freeLibrary();

	void handleVSMessage(int a_messageType, const QString & a_message);

	friend void VS_CC vsMessageHandler(int a_msgType,
		const char * a_message, void * a_pUserData);

	SettingsManagerCore * m_pSettingsManager;

	QLibrary m_vsScriptLibrary;

    FNP_vssGetVSScriptAPI vssGetVSScriptAPI;

	bool m_vsScriptInitialized;

	bool m_initialized;

    VSCore * m_pVSEditCore;
    const VSAPI * m_cpVSAPI;
    const VSSCRIPTAPI * m_cpVSScriptAPI;
};

//==============================================================================

#endif // VS_SCRIPT_LIBRARY_H_INCLUDED
