#include "vs_script_processor_structures.h"

//==============================================================================

Frame::Frame(int a_number, int a_outputIndex,
    const VSFrame * a_cpOutputFrame,
    const VSFrame * a_cpPreviewFrame, const VSFrame *a_cpWaveformFrame, const VSFrame *a_cpRGBParadeFrame):
	  number(a_number)
	, outputIndex(a_outputIndex)
    , cpOutputFrame(a_cpOutputFrame)
    , cpPreviewFrame(a_cpPreviewFrame)
    , cpWaveformFrame(a_cpWaveformFrame)
    , cpRGBParadeFrame(a_cpRGBParadeFrame)
{
}

bool Frame::operator==(const Frame & a_other) const
{
	return ((number == a_other.number) && (outputIndex == a_other.outputIndex));
}

//==============================================================================

FrameTicket::FrameTicket()
{
}

FrameTicket::FrameTicket(int a_frameNumber, int a_outputIndex,
        VSNode * a_pOutputNode, bool a_needPreview, bool a_playing, int a_scopeNum,
        VSNode * a_pPreviewNode, VSNode *a_pWaveformNode, VSNode *a_pRGBParadeNode, PreviewFilters a_previewFilters):
	frameNumber(a_frameNumber)
	, outputIndex(a_outputIndex)
	, pOutputNode(a_pOutputNode)
	, needPreview(a_needPreview)
    , playing(a_playing)
    , scopeNum(a_scopeNum)
	, pPreviewNode(a_pPreviewNode)
    , pWaveformNode(a_pWaveformNode)
    , pRGBParadeNode(a_pRGBParadeNode)
    , cpOutputFrame(nullptr)
    , cpPreviewFrame(nullptr)
    , cpScopeFrame(nullptr)
    , previewFilters(a_previewFilters)
	, discard(false)
{
}

//==============================================================================

bool FrameTicket::isComplete() const
{
    bool complete = (cpOutputFrame != nullptr);
	if(needPreview)
        complete = complete && (cpPreviewFrame != nullptr);
    return complete;
}

bool FrameTicket::isPlaying() const
{
    return ((needPreview && cpPreviewFrame != nullptr) && playing == true);
}

//==============================================================================

NodePair::NodePair():
	  outputIndex(-1)
	, pOutputNode(nullptr)
	, pPreviewNode(nullptr)
    , pWaveformNode(nullptr)
    , pRGBParadeNode(nullptr)
{
}

//==============================================================================

NodePair::NodePair(int a_outputIndex, VSNode * a_pOutputNode,
    VSNode * a_pPreviewNode):
    outputIndex(a_outputIndex),
    pOutputNode(a_pOutputNode),
    pPreviewNode(a_pPreviewNode)
{
}

//==============================================================================

bool NodePair::isNull() const
{
	return ((outputIndex == -1) && (pOutputNode == nullptr) &&
		(pPreviewNode == nullptr));
}

//==============================================================================

bool NodePair::isValid() const
{
	return ((outputIndex >= 0) && (pOutputNode != nullptr) &&
		(pPreviewNode != nullptr));
}

//==============================================================================

PreviewFilters::PreviewFilters():
    colorMatrix(YuvMatrixCoefficients::m709),
    planeActive(false),
    updateFilters(false)
{

}

PreviewFilters::PreviewFilters(YuvMatrixCoefficients a_yuvMatrixCoefficient,
                               bool a_planeActive, YUVPlane a_plane):
    colorMatrix(a_yuvMatrixCoefficient),
    planeActive(a_planeActive),
    plane(a_plane),
    updateFilters(false)
{
}

bool PreviewFilters::operator==(const PreviewFilters &a_other) const
{
    return ((colorMatrix == a_other.colorMatrix) && (plane == a_other.plane)
            && planeActive == a_other.planeActive);
}

bool PreviewFilters::operator!=(const PreviewFilters &a_other) const
{
    return ((colorMatrix != a_other.colorMatrix) || (plane != a_other.plane)
            || planeActive != a_other.planeActive);
}
