#include "vapoursynth_script_processor.h"

#include "../helpers.h"
#include "vs_script_library.h"
#include <vapoursynth/VSConstants4.h>
#include <vapoursynth/VSHelper4.h>

#include <vector>
#include <cmath>
#include <utility>
#include <memory>
#include <functional>
#include <sstream>

//==============================================================================

/* callback function for VSAPI->getFrameAsync() */
void VS_CC frameReady(void * a_pUserData,
    const VSFrame * a_cpFrame, int a_frameNumber,
    VSNode * a_pNode, const char * a_errorMessage)
{
	VapourSynthScriptProcessor * pScriptProcessor =
		static_cast<VapourSynthScriptProcessor *>(a_pUserData);
	Q_ASSERT(pScriptProcessor);
	QString errorMessage(a_errorMessage);
	QMetaObject::invokeMethod(pScriptProcessor,
		"slotReceiveFrameAndProcessQueue",
		Qt::QueuedConnection,
        Q_ARG(const VSFrame *, a_cpFrame),
		Q_ARG(int, a_frameNumber),
        Q_ARG(VSNode *, a_pNode),
        Q_ARG(QString, errorMessage));
}

// END OF void VS_CC frameReady(void * a_pUserData,
//	const VSFrameRef * a_cpFrameRef, int a_frameNumber,
//	VSNodeRef * a_pNodeRef, const char * a_errorMessage)
//==============================================================================

VapourSynthScriptProcessor::VapourSynthScriptProcessor(
	SettingsManagerCore * a_pSettingsManager,
	VSScriptLibrary * a_pVSScriptLibrary,
	QObject * a_pParent):
	QObject(a_pParent)
	, m_pSettingsManager(a_pSettingsManager)
	, m_pVSScriptLibrary(a_pVSScriptLibrary)
	, m_script()
	, m_scriptName()
	, m_error()
    , m_initialized(false)
    , m_pCore(nullptr)
	, m_cpVSAPI(nullptr)
    , m_cpVSScriptAPI(nullptr)
	, m_pVSScript(nullptr)
    , m_cpVideoInfo(nullptr)
    , m_cpCoreInfo()
	, m_finalizing(false)
{
	Q_ASSERT(m_pSettingsManager);
	Q_ASSERT(m_pVSScriptLibrary);

}

// END OF VapourSynthScriptProcessor::VapourSynthScriptProcessor(
//		SettingsManagerCore * a_pSettingsManager,
//		VSScriptLibrary * a_pVSScriptLibrary, QObject * a_pParent)
//==============================================================================

VapourSynthScriptProcessor::~VapourSynthScriptProcessor()
{
	finalize();
}

// END OF VapourSynthScriptProcessor::~VapourSynthScriptProcessor()
//==============================================================================

bool VapourSynthScriptProcessor::initialize(const QString& a_script,
	const QString& a_scriptName)
{
	if(m_initialized || m_finalizing)
	{
        m_error = tr("Script processor is already in use.");
		emit signalWriteLogMessage(mtCritical, m_error);
		return false;
	}

    // get api from vsscript library
    m_cpVSScriptAPI = m_pVSScriptLibrary->getVSScriptAPI();
    m_cpVSAPI = m_pVSScriptLibrary->getVSAPI();

    m_pVSScript = m_cpVSScriptAPI->createScript(nullptr);
    m_pCore = m_cpVSScriptAPI->getCore(m_pVSScript);

    // try out the evaluateFile later, it can be more convience.
    int loadSuccess = m_cpVSScriptAPI->evaluateBuffer(m_pVSScript,
        a_script.toUtf8().constData(), a_scriptName.toUtf8().constData());

    if (loadSuccess != 0) {
        const char * vsError = m_cpVSScriptAPI->getError(m_pVSScript);
        emit signalWriteLogMessage(mtCritical, QString(vsError));
        return false;
    }

    m_cpVSAPI->getCoreInfo(m_pCore, &m_cpCoreInfo);

    if(m_cpCoreInfo.core < 55)
	{
        m_error = tr("VapourSynth R55+ required for preview.");
		emit signalWriteLogMessage(mtCritical, m_error);
        finalize();
        return false;
	}

    VSNode * pOutputNode = m_cpVSScriptAPI->getOutputNode(m_pVSScript, 0);
    if(!pOutputNode) // may want to try if(outputnode == NULL)
	{
        m_error = tr("Failed to get the script output node.");
		emit signalWriteLogMessage(mtCritical, m_error);
		finalize();
		return false;
	}

    m_cpVideoInfo = m_cpVSAPI->getVideoInfo(pOutputNode); // excessive video info?
	m_cpVSAPI->freeNode(pOutputNode);

	m_script = a_script;
	m_scriptName = a_scriptName;

    m_error.clear();
	m_initialized = true;

	sendFrameQueueChangeSignal();

	return true;
}

// END OF bool VapourSynthScriptProcessor::initialize(const QString& a_script,
//		const QString& a_scriptName)
//==============================================================================

bool VapourSynthScriptProcessor::finalize()
{
	m_finalizing = true;
	bool noFrameTicketsInProcess = flushFrameTicketsQueue();

	if(!noFrameTicketsInProcess)
		return false;

    flushNodePairMap();

	m_cpVideoInfo = nullptr;

    if(m_pVSScript) {
        m_cpVSScriptAPI->freeScript(m_pVSScript);
        m_pVSScript = nullptr;
    }

    m_script.clear();
	m_scriptName.clear();

	m_initialized = false;
	m_finalizing = false;

	emit signalFinalized();

	return true;
}

// END OF bool VapourSynthScriptProcessor::finalize()
//==============================================================================

bool VapourSynthScriptProcessor::isInitialized() const
{
	return m_initialized;
}

// END OF bool VapourSynthScriptProcessor::isInitialized() const
//==============================================================================

QString VapourSynthScriptProcessor::error() const
{
	return m_error;
}

// END OF QString VapourSynthScriptProcessor::error() const
//==============================================================================

const VSVideoInfo * VapourSynthScriptProcessor::getVideoInfo()
{
	if(!m_initialized)
		return nullptr;

    return m_cpVideoInfo;
}

// END OF const VSVideoInfo * VapourSynthScriptProcessor::videoInfo() const
//==============================================================================

bool VapourSynthScriptProcessor::requestFrameAsync(int a_frameNumber,
    int a_outputIndex, bool a_needPreview, bool a_isPlaying, int a_scopeNum, PreviewFilters a_previewFilters)
{
    /* This function pumps out frame tickets into a que,
     * then the que will be processed later by processFrameTicketsQueue()
    */
    if(!m_initialized)
        return false;

    if(a_frameNumber < 0)
    {
        m_error = tr("Requested frame number %1 is negative.")
            .arg(a_frameNumber);
		emit signalWriteLogMessage(mtCritical, m_error);
		return false;
	}

	Q_ASSERT(m_cpVSAPI);

    NodePair & nodePair = getNodePair(a_outputIndex, a_needPreview, a_previewFilters);
	if(!nodePair.pOutputNode)
		return false;

	const VSVideoInfo * cpVideoInfo =
		m_cpVSAPI->getVideoInfo(nodePair.pOutputNode);
    Q_ASSERT(cpVideoInfo);

    if(a_frameNumber >= cpVideoInfo->numFrames)
	{
        m_error = tr("Requested frame number %1 is outside the frame "
            "range.").arg(a_frameNumber);
		emit signalWriteLogMessage(mtCritical, m_error);
		return false;
	}

	if(a_needPreview && (!nodePair.pPreviewNode))
		return false;

	FrameTicket newFrameTicket(a_frameNumber, a_outputIndex,
        nodePair.pOutputNode, a_needPreview, a_isPlaying, a_scopeNum,
        nodePair.pPreviewNode, nodePair.pWaveformNode, nodePair.pRGBParadeNode);

	m_frameTicketsQueue.push_back(newFrameTicket);
    sendFrameQueueChangeSignal(); // send signal to update the status icon
	processFrameTicketsQueue();

	return true;
}

// END OF void VapourSynthScriptProcessor::requestFrameAsync(int a_frameNumber,
//		int a_outputIndex, bool a_needPreview)
//==============================================================================

bool VapourSynthScriptProcessor::flushFrameTicketsQueue()
{
    // add discard flag to all tickets
	for(FrameTicket & ticket : m_frameTicketsInProcess)
		ticket.discard = true;

	size_t queueSize = m_frameTicketsQueue.size();

    m_frameTicketsQueue.clear();
	if(queueSize)
		sendFrameQueueChangeSignal();

	return m_frameTicketsInProcess.empty();
}

// END OF bool VapourSynthScriptProcessor::flushFrameTicketsQueue()
//==============================================================================

void VapourSynthScriptProcessor::flushNodePairMap()
{
    for(auto &mapItem : m_nodePairMap)
    {
        NodePair & nodePair = mapItem.second;
        if(nodePair.pOutputNode)
            m_cpVSAPI->freeNode(nodePair.pOutputNode);
        if(nodePair.pPreviewNode)
            m_cpVSAPI->freeNode(nodePair.pPreviewNode);
        if(nodePair.pWaveformNode)
            m_cpVSAPI->freeNode(nodePair.pWaveformNode);
        if(nodePair.pRGBParadeNode)
            m_cpVSAPI->freeNode(nodePair.pRGBParadeNode);
    }
    m_nodePairMap.clear();
}

// END OF bool VapourSynthScriptProcessor::flushNodePairMap()
//==============================================================================

const QString & VapourSynthScriptProcessor::script() const
{
	return m_script;
}

// END OF const QString & VapourSynthScriptProcessor::script() const
//==============================================================================

const QString & VapourSynthScriptProcessor::scriptName() const
{
	return m_scriptName;
}

// END OF const QString & VapourSynthScriptProcessor::scriptName() const
//==============================================================================

void VapourSynthScriptProcessor::setScriptName(const QString & a_scriptName)
{
	m_scriptName = a_scriptName;
}

// END OF void VapourSynthScriptProcessor::setScriptName(
//		const QString & a_scriptName)
//==============================================================================

void VapourSynthScriptProcessor::slotReceiveFrameAndProcessQueue(
    const VSFrame * a_cpFrame, int a_frameNumber, VSNode * a_pNode,
    QString a_errorMessage)
{
    receiveFrame(a_cpFrame, a_frameNumber, a_pNode, a_errorMessage);
	processFrameTicketsQueue();
}

// END OF void VapourSynthScriptProcessor::slotReceiveFrameAndProcessQueue(
//		const VSFrameRef * a_cpFrameRef, int a_frameNumber,
//		VSNodeRef * a_pNodeRef, QString a_errorMessage)
//==============================================================================

void VapourSynthScriptProcessor::slotApplyPreviewFilters(PreviewFilters & a_previewFilters)
{
    for(std::pair<const int, NodePair> & mapItem : m_nodePairMap)
    {
        NodePair & nodePair = mapItem.second;

        if(nodePair.pPreviewNode) {
            bool previewNodeCreated = recreatePreviewNode(nodePair, a_previewFilters);
            if(!previewNodeCreated)
            {
                m_error = tr("Couldn't create preview node from preview filters");
                emit signalWriteLogMessage(mtCritical, m_error);
                return;
            }
        }
    }
}

void VapourSynthScriptProcessor::receiveFrame(
    const VSFrame * a_cpFrame, int a_frameNumber,
    VSNode * a_pNode, const QString & a_errorMessage)
{
    /* pack received frame into ticket */
	Q_ASSERT(m_cpVSAPI);

	if(!a_errorMessage.isEmpty())
	{
        m_error = tr("Error on frame %1 request:\n%2")
			.arg(a_frameNumber).arg(a_errorMessage);
		emit signalWriteLogMessage(mtCritical, m_error);
	}

	FrameTicket ticket(a_frameNumber, -1, nullptr);

    /* Find the matching frame ticket in the que for the returning frame and check for needPreview tag.
     * If preview is needed, request the frame again using the preview node
    */
    auto it = std::find_if(
		m_frameTicketsInProcess.begin(), m_frameTicketsInProcess.end(),
		[&](const FrameTicket & a_ticket)
		{
			return ((a_ticket.frameNumber == a_frameNumber) &&
                ((a_ticket.pOutputNode == a_pNode) ||
                (a_ticket.pPreviewNode == a_pNode) ||
                (a_ticket.pWaveformNode == a_pNode) ||
                (a_ticket.pRGBParadeNode == a_pNode)));
		});

    // pack returned frames into frame ticket
    // this will fire getFrameAsync again to get the previewFrame
	if(it != m_frameTicketsInProcess.end())
	{
        if(it->pOutputNode == a_pNode)
		{
            it->cpOutputFrame = a_cpFrame;
			m_cpVSAPI->freeNode(it->pOutputNode);
            it->pOutputNode = nullptr;

			if(it->needPreview)
			{
				Q_ASSERT(it->pPreviewNode);
                if(a_cpFrame)
				{
					m_cpVSAPI->getFrameAsync(it->frameNumber, it->pPreviewNode,
						frameReady, this);
				}
				else
				{
					m_cpVSAPI->freeNode(it->pPreviewNode);
					it->pPreviewNode = nullptr;
				}
			}
		}
        else if(it->pPreviewNode == a_pNode)
        {
            it->cpPreviewFrame = a_cpFrame;
            m_cpVSAPI->freeNode(it->pPreviewNode);
            it->pPreviewNode = nullptr;
        }
        else if(it->pWaveformNode == a_pNode) {
            it->cpScopeFrame = a_cpFrame;

            // do not free waveform node and rgb parade node here
        }
        else if(it->pRGBParadeNode == a_pNode) {
            it->cpScopeFrame = a_cpFrame;
        }

        // free outputNode if video is playing
        if (it->pOutputNode != nullptr) {
            m_cpVSAPI->freeNode(it->pOutputNode);
            it->pOutputNode = nullptr;
        }

        // free output node and preview node in ticket before distributing frames
        if((it->pOutputNode != nullptr) ||
           (it->needPreview && (it->pPreviewNode != nullptr)))
            return;

        // if scope dialog is open, check if scope frame exist before distributing frames.
        if (it->scopeNum > 0) {
            if (it->cpScopeFrame == nullptr)
                return;
        }

		ticket = *it;
		m_frameTicketsInProcess.erase(it);
		sendFrameQueueChangeSignal();
	}
	else
	{
        QString warning = tr("Warning: received frame not registered in "
			"processing. Frame number: %1; Node: %2\n")
            .arg(a_frameNumber).arg(intptr_t(a_pNode));
		emit signalWriteLogMessage(mtCritical, warning);
        m_cpVSAPI->freeFrame(a_cpFrame);
	}

	if(!ticket.discard)
	{
        if(ticket.isComplete() || ticket.isPlaying())
        {
			emit signalDistributeFrame(ticket.frameNumber, ticket.outputIndex,
                ticket.cpOutputFrame, ticket.cpPreviewFrame, ticket.cpScopeFrame);
		}
		else
		{
			emit signalFrameRequestDiscarded(ticket.frameNumber,
				ticket.outputIndex, QString());
		}
	}

	freeFrameTicket(ticket);
}

// END OF void VapourSynthScriptProcessor::receiveFrame(
//		const VSFrameRef * a_cpFrameRef, int a_frameNumber,
//		VSNodeRef * a_pNodeRef, const QString & a_errorMessage)
//==============================================================================

void VapourSynthScriptProcessor::processFrameTicketsQueue()
{
	Q_ASSERT(m_cpVSAPI);

	size_t oldInQueue = m_frameTicketsQueue.size();
    size_t oldInProcess = size_t(m_frameTicketsInProcess.size());

    /* move frame ticket from queue to inProcess */
    while((int(m_frameTicketsInProcess.size()) < m_cpCoreInfo.numThreads) &&
        (!m_frameTicketsQueue.empty()))
    {
		FrameTicket ticket = std::move(m_frameTicketsQueue.front());
		m_frameTicketsQueue.pop_front();

		// In case preview node was hot-swapped.
		NodePair & nodePair =
            getNodePair(ticket.outputIndex, ticket.needPreview, ticket.previewFilters);

		bool validPair = (nodePair.pOutputNode != nullptr);
		if(ticket.needPreview)
			validPair = validPair && (nodePair.pPreviewNode != nullptr);
		if(!validPair)
		{
            QString reason = tr("No nodes to produce the frame "
				"%1 at output index %2.").arg(ticket.frameNumber)
				.arg(ticket.outputIndex);
			emit signalFrameRequestDiscarded(ticket.frameNumber,
				ticket.outputIndex, reason);
			continue;
		}

        ticket.pOutputNode = m_cpVSAPI->addNodeRef(nodePair.pOutputNode);
        if(ticket.needPreview) {
            ticket.pPreviewNode = m_cpVSAPI->addNodeRef(nodePair.pPreviewNode);
            ticket.pWaveformNode = m_cpVSAPI->addNodeRef(nodePair.pWaveformNode);
            ticket.pRGBParadeNode = m_cpVSAPI->addNodeRef(nodePair.pRGBParadeNode);
        }

        // get only preview frame if the video is playing
        if(ticket.playing) {
            m_cpVSAPI->getFrameAsync(ticket.frameNumber, ticket.pPreviewNode,
                frameReady, this);
        } else {
            m_cpVSAPI->getFrameAsync(ticket.frameNumber, ticket.pOutputNode,
                frameReady, this);
        }

        switch(ticket.scopeNum) {
            case 1:
                m_cpVSAPI->getFrameAsync(ticket.frameNumber, ticket.pWaveformNode, frameReady, this);
                break;
            case 2:
                m_cpVSAPI->getFrameAsync(ticket.frameNumber, ticket.pRGBParadeNode, frameReady, this);
                break;
            default:
                break;
        }

		m_frameTicketsInProcess.push_back(ticket);
	}

	size_t inQueue = m_frameTicketsQueue.size();
    size_t inProcess = size_t(m_frameTicketsInProcess.size());
	if((inQueue != oldInQueue) || (oldInProcess != inProcess))
		sendFrameQueueChangeSignal();

	if(m_finalizing)
		finalize();
}

// END OF void VapourSynthScriptProcessor::processFrameTicketsQueue()
//==============================================================================

void VapourSynthScriptProcessor::sendFrameQueueChangeSignal()
{
	size_t inQueue = m_frameTicketsQueue.size();
    size_t inProcess = size_t(m_frameTicketsInProcess.size());
    size_t maxThreads = size_t(m_cpCoreInfo.numThreads);
	emit signalFrameQueueStateChanged(inQueue, inProcess, maxThreads);
}

// END OF void VapourSynthScriptProcessor::sendFrameQueueChangeSignal()
//==============================================================================

bool VapourSynthScriptProcessor::recreatePreviewNode(NodePair & a_nodePair, PreviewFilters a_previewFilters)
{
    /* request an output node with RGB */
    if(!a_nodePair.pOutputNode)
        return false;

    if(!m_cpVSAPI)
		return false;

	if(a_nodePair.pPreviewNode)
	{
		m_cpVSAPI->freeNode(a_nodePair.pPreviewNode);
	}

	const VSVideoInfo * cpVideoInfo =
		m_cpVSAPI->getVideoInfo(a_nodePair.pOutputNode);
	if(!cpVideoInfo)
		return false;
    const VSVideoFormat cpVideoFormat = cpVideoInfo->format;
    int videoWidth = cpVideoInfo->width;
    int videoHeight = cpVideoInfo->height;

    VSNode * pProcessingNode = m_cpVSAPI->addNodeRef(a_nodePair.pOutputNode);

    // applying preview filters
    // plane filter has higher presedence
    // if plane filter is active, color matrix filter will be disable
    if(a_previewFilters.updateFilters && cpVideoFormat.colorFamily != cfGray) {
        if(a_previewFilters.planeActive) {

            // convert to gray scale, if outputnode is not YUV, convert it to YUV first
            if(cpVideoFormat.colorFamily != cfYUV)
                pProcessingNode = invokeResizeConversion(pProcessingNode, pfYUV420P8);

            // converting to Y/U/V plane using gray scale
            pProcessingNode = invokeShufflePlanes(pProcessingNode, int(a_previewFilters.plane));

            // resize UV plane to match input width/height
            if(a_previewFilters.plane == YUVPlane::U || a_previewFilters.plane == YUVPlane::V) {
                const VSVideoInfo * cpNodeVideoInfo = m_cpVSAPI->getVideoInfo(pProcessingNode);
                int nodeWidth = cpNodeVideoInfo->width;
                int nodeHeight = cpNodeVideoInfo->height;

                if (nodeWidth != videoWidth || nodeHeight != videoHeight)
                    pProcessingNode = invokeResize(pProcessingNode, videoWidth, videoHeight);

                // check for akarin plugin
                if (m_pSettingsManager->loadedRequiredPlugins.contains("akarin")) {

                    // auto levels the U/V plane
                    pProcessingNode = invokePlaneStats(pProcessingNode);

                    auto min_in = "x.PlaneStatsMin";
                    auto max_in = "x.PlaneStatsMax";
                    auto min_out = 16;
                    auto max_out = 235;

                    std::ostringstream expr_stream {};
                    expr_stream << "x " << min_in << " - " << max_in << " " << min_in << " - / " << max_out - min_out << " * " << min_out << " +";
                    auto expr = expr_stream.str();

                    pProcessingNode = invokeAkarinExpr(pProcessingNode, expr);
                }
            }

            // convert to rgb24 for preview
            pProcessingNode = invokeResizeConversion(pProcessingNode, pfRGB24);

        } else {
            // color matrix
            pProcessingNode = invokeResizeConversion(pProcessingNode,
                pfRGB24, a_previewFilters.colorMatrix);
        }
    } else {
        // default with no preview filters
        bool isVideoRGB24 = vsh::isSameVideoPresetFormat(pfRGB24, &cpVideoFormat, m_pCore, m_cpVSAPI);

        if (!isVideoRGB24)
            pProcessingNode = invokeResizeConversion(pProcessingNode, pfRGB24);
    }

    // packing rgb planar, new for vsapi4
    pProcessingNode = invokeLibP2PPack(pProcessingNode);

    if (pProcessingNode == nullptr) {
        return false;
    }

    a_nodePair.pPreviewNode = pProcessingNode;
    return true;
}

bool VapourSynthScriptProcessor::createWaveformNode(NodePair &a_nodePair)
{
    if(!a_nodePair.pOutputNode)
        return false;

    if(!m_cpVSAPI)
        return false;

    if (a_nodePair.pWaveformNode) {
        m_cpVSAPI->freeNode(a_nodePair.pWaveformNode);
    }

    VSNode * pProcessingNode = m_cpVSAPI->addNodeRef(a_nodePair.pOutputNode);

    // for gray scale input, convert to yuv first
    const VSVideoInfo * cpVideoInfo = m_cpVSAPI->getVideoInfo(pProcessingNode);
    if(!cpVideoInfo) return false;
    const VSVideoFormat cpVideoFormat = cpVideoInfo->format;

    if(cpVideoFormat.colorFamily != cfYUV)
        pProcessingNode = invokeResizeConversion(pProcessingNode, pfYUV420P8);

    pProcessingNode = invokeHistogram(pProcessingNode, "Classic");
    pProcessingNode = invokeResizeConversion(pProcessingNode, pfRGB24);
    pProcessingNode = invokeLibP2PPack(pProcessingNode);

    if (pProcessingNode == nullptr) {
        return false;
    }

    a_nodePair.pWaveformNode = pProcessingNode;
    return true;
}

bool VapourSynthScriptProcessor::createRGBParadeNode(NodePair &a_nodePair)
{
    if(!a_nodePair.pOutputNode)
        return false;

    if(!m_cpVSAPI)
        return false;

    if (a_nodePair.pRGBParadeNode) {
        m_cpVSAPI->freeNode(a_nodePair.pRGBParadeNode);
    }

    VSNode * pProcessingNode = m_cpVSAPI->addNodeRef(a_nodePair.pOutputNode);

    // convert to RGB24
    auto RGBNode = invokeResizeConversion(pProcessingNode, pfRGB24);

    // extract RGB planes      
    auto planesMap = invokeSplitPlanes(RGBNode);

    auto RNode = m_cpVSAPI->mapGetNode(planesMap, "clip", 0, nullptr);
    auto GNode = m_cpVSAPI->mapGetNode(planesMap, "clip", 1, nullptr);
    auto BNode = m_cpVSAPI->mapGetNode(planesMap, "clip", 2, nullptr);

    m_cpVSAPI->freeMap(planesMap);

    // convert back to YUV
    RNode = invokeResizeConversion(RNode, pfYUV420P8);
    GNode = invokeResizeConversion(GNode, pfYUV420P8);
    BNode = invokeResizeConversion(BNode, pfYUV420P8);

    // convert to RGB waveform
    RNode = invokeHistogram(RNode, "Classic");
    GNode = invokeHistogram(GNode, "Classic");
    BNode = invokeHistogram(BNode, "Classic");

    // stack the RGB nodes
    pProcessingNode = invokeStackHorizontal({RNode, GNode, BNode});

    pProcessingNode = invokeResizeConversion(pProcessingNode, pfRGB24);
    pProcessingNode = invokeLibP2PPack(pProcessingNode);

    if (pProcessingNode == nullptr) {
        return false;
    }

    a_nodePair.pRGBParadeNode = pProcessingNode;
    return true;
}

// END OF bool VapourSynthScriptProcessor::recreatePreviewNode(
//		NodePair & a_nodePair)
//==============================================================================

void VapourSynthScriptProcessor::freeFrameTicket(FrameTicket & a_ticket)
{
	Q_ASSERT(m_cpVSAPI);

	a_ticket.discard = true;

    if(a_ticket.cpOutputFrame)
	{
        m_cpVSAPI->freeFrame(a_ticket.cpOutputFrame);
        a_ticket.cpOutputFrame = nullptr;
	}

    if(a_ticket.cpPreviewFrame)
	{
        m_cpVSAPI->freeFrame(a_ticket.cpPreviewFrame);
        a_ticket.cpPreviewFrame = nullptr;
	}

    if(a_ticket.cpScopeFrame)
    {
        m_cpVSAPI->freeFrame(a_ticket.cpScopeFrame);
        a_ticket.cpScopeFrame = nullptr;
    }

    if(a_ticket.pOutputNode)
    {
        m_cpVSAPI->freeNode(a_ticket.pOutputNode);
        a_ticket.pOutputNode = nullptr;
    }

    if(a_ticket.pPreviewNode)
    {
        m_cpVSAPI->freeNode(a_ticket.pPreviewNode);
        a_ticket.pPreviewNode = nullptr;
    }

    if(a_ticket.pWaveformNode)
    {
        m_cpVSAPI->freeNode(a_ticket.pWaveformNode);
        a_ticket.pWaveformNode = nullptr;
    }

    if(a_ticket.pRGBParadeNode)
    {
        m_cpVSAPI->freeNode(a_ticket.pRGBParadeNode);
        a_ticket.pRGBParadeNode = nullptr;
    }
}

// END OF void VapourSynthScriptProcessor::freeFrameTicket(
//		FrameTicket & a_ticket)
//==============================================================================

NodePair & VapourSynthScriptProcessor::getNodePair(int a_outputIndex,
    bool a_needPreview, PreviewFilters a_previewFilters)
{
    // retrieve multiple video nodes from vsscript library:
    //  - outputNode to retrieve general info of the video,
    //  - previewNode to convert to RGB for display
    //  - waveformNode and RGBParadeNode for scope
    NodePair & nodePair = m_nodePairMap[a_outputIndex];

    // create outputnode here for the first time
    if(!nodePair.pOutputNode)
    {
        Q_ASSERT(!nodePair.pPreviewNode);

		nodePair.pOutputNode =
            m_cpVSScriptAPI->getOutputNode(m_pVSScript, a_outputIndex);

		if(!nodePair.pOutputNode)
		{
            m_error = tr("Couldn't resolve output node number %1.")
				.arg(a_outputIndex);
			emit signalWriteLogMessage(mtCritical, m_error);
			return nodePair;
		}
	}

    /* if preview flag is true and no preview in nodepair yet, create one */
	if(a_needPreview && (!nodePair.pPreviewNode))
	{
        bool previewNodeCreated = recreatePreviewNode(nodePair, a_previewFilters);
		if(!previewNodeCreated)
		{
            m_error = tr("Couldn't create preview node for output "
				"number %1.").arg(a_outputIndex);
			emit signalWriteLogMessage(mtCritical, m_error);
			return nodePair;
		}

        bool waveformNodeCreated = createWaveformNode(nodePair);
        if(!waveformNodeCreated)
        {
            m_error = tr("Couldn't create waveform node for output "
                "number %1.").arg(a_outputIndex);
            emit signalWriteLogMessage(mtCritical, m_error);
            return nodePair;
        }

        bool RGBParadeNodeCreated = createRGBParadeNode(nodePair);
        if(!RGBParadeNodeCreated)
        {
            m_error = tr("Couldn't create RGB parade node for output "
                "number %1.").arg(a_outputIndex);
            emit signalWriteLogMessage(mtCritical, m_error);
            return nodePair;
        }
	}
	return nodePair;
}

// END OF NodePair VapourSynthScriptProcessor::getNodePair(int a_outputIndex,
//		bool a_needPreview)
//==============================================================================

QString VapourSynthScriptProcessor::framePropsString(
    const VSFrame * a_cpFrame) const
{
    if(!a_cpFrame)
        return tr("Null frame.");

    Q_ASSERT(m_cpVSAPI);

    QString propsString;
    QStringList propsStringList;

    std::map<char, QString> propTypeToString =
    {
        {ptUnset, "<unset>"},
        {ptInt, "int"},
        {ptFloat, "float"},
        {ptData, "data"},
        {ptVideoNode, "node"},
        {ptVideoFrame, "frame"},
        {ptFunction, "function"},
    };

    const VSMap * cpProps = m_cpVSAPI->getFramePropertiesRO(a_cpFrame);

    int propsNumber = m_cpVSAPI->mapNumKeys(cpProps);
    int error;
    for(int i = 0; i < propsNumber; ++i)
    {
        const char * propKey = m_cpVSAPI->mapGetKey(cpProps, i);
        if(!propKey)
            continue;
        QString currentPropString = QString("%1 : ").arg(propKey);
        int propType = m_cpVSAPI->mapGetType(cpProps, propKey);

        if (QString(propKey) == "_ColorRange") {
            int64_t colorRange = m_cpVSAPI->mapGetInt(cpProps, propKey, 0, &error);
            if (error) break;
            static QMap<int, QString> colorRangeMap = {
                {VSC_RANGE_FULL, "full range"},
                {VSC_RANGE_LIMITED, "limited range"}
            };
            currentPropString += colorRangeMap[int(colorRange)];

        } else
        if (QString(propKey) == "_FieldBased") {
            int64_t fieldBased = m_cpVSAPI->mapGetInt(cpProps, propKey, 0, &error);
            if (error) break;
            static QMap<int, QString> fieldBasedMap = {
                {VSC_FIELD_PROGRESSIVE, "frame based (progressive)"},
                {VSC_FIELD_BOTTOM, "bottom field first"},
                {VSC_FIELD_TOP, "top field first"}
            };
            currentPropString += fieldBasedMap[int(fieldBased)];

        } else
        if (QString(propKey) == "_ChromaLocation") {
            int64_t chromaLocation = m_cpVSAPI->mapGetInt(cpProps, propKey, 0, &error);
            if (error) break;
            static QMap<int, QString> chromaLocationMap = {
                {VSC_CHROMA_LEFT, "left"},
                {VSC_CHROMA_CENTER, "center"},
                {VSC_CHROMA_TOP_LEFT, "top left"},
                {VSC_CHROMA_TOP, "top"},
                {VSC_CHROMA_BOTTOM_LEFT, "bottom left"},
                {VSC_CHROMA_BOTTOM, "bottom"}
            };
            currentPropString += chromaLocationMap[int(chromaLocation)];

        } else
        if (QString(propKey) == "_Matrix") {
            int64_t matrix = m_cpVSAPI->mapGetInt(cpProps, propKey, 0, &error);
            if (error) break;
            static QMap<int, QStringList> matrixMap = {
                {VSC_MATRIX_RGB, {"sRGB"}},
                {VSC_MATRIX_BT709, {"BT.709"}},
                {VSC_MATRIX_UNSPECIFIED, {"Unspecified"}},
                {VSC_MATRIX_FCC, {"FCC"}},
                {VSC_MATRIX_BT470_BG, {"BT.470 BG"}},
                {VSC_MATRIX_ST170_M, {"SMPTE ST 170M"}},
                {VSC_MATRIX_ST240_M, {"SMPTE ST 240M"}},
                {VSC_MATRIX_YCGCO, {"YCGCO"}},
                {VSC_MATRIX_BT2020_NCL, {"BT.2020 NCL"}},
                {VSC_MATRIX_BT2020_CL, {"BT.2020 CL"}},
                {VSC_MATRIX_CHROMATICITY_DERIVED_NCL, {"Chromaticity derived NCL"}},
                {VSC_MATRIX_CHROMATICITY_DERIVED_CL, {"Chromaticity derived CL"}},
                {VSC_MATRIX_ICTCP, {"ICTCP"}}
            };
            currentPropString += matrixMap[int(matrix)].join(", ");
        } else
        if (QString(propKey) == "_PictType") {
            QString pictType = m_cpVSAPI->mapGetData(cpProps, propKey, 0, &error);
            if (error) break;
            currentPropString += pictType;
        } else {

            int elementsNumber = m_cpVSAPI->mapNumElements(cpProps, propKey);
            if(elementsNumber > 1)
                currentPropString += "[]";
            switch(propType)
            {
            case ptVideoFrame:
            case ptVideoNode:
            case ptFunction:
                break;
            case ptUnset:
                currentPropString += ": <unset>";
                break;
            case ptInt:
            case ptFloat:
            case ptData:
            {
//                currentPropString += " : ";
                QStringList elementStringList;
                for(int j = 0; j < elementsNumber; ++j)
                {
                    QString elementString;
                    int error;
                    if(propType == ptInt)
                    {
                        int64_t element = m_cpVSAPI->mapGetInt(cpProps,
                            propKey, j, &error);
                        if(error)
                            elementString = "<error>";
                        else
                            elementString = QString::number(element);
                    }
                    else if(propType == ptFloat)
                    {
                        double element = m_cpVSAPI->mapGetFloat(cpProps,
                            propKey, j, &error);
                        if(error)
                            elementString = "<error>";
                        else
                            elementString = QString::number(element);
                    }
                    else if(propType == ptData)
                    {
                        const char * element = m_cpVSAPI->mapGetData(cpProps,
                            propKey, j, &error);
                        if(error)
                            elementString = "<error>";
                        else
                            elementString = QString::fromUtf8(element);
                    }

                    elementStringList += elementString;
                }
                currentPropString += elementStringList.join(", ");
                break;
            }
            default:
                Q_ASSERT(false);
            }

        }

        propsStringList += currentPropString;
    }

    propsString = propsStringList.join("\n");
    return propsString;
}

// END OF QString VapourSynthScriptProcessor::framePropsString(
//		const VSFrameRef * a_cpFrame) const
//==============================================================================

void VapourSynthScriptProcessor::printFrameProps(const VSFrame * a_cpFrame)
{
    QString message = tr("Frame properties:\n%1")
		.arg(framePropsString(a_cpFrame));
    emit signalWriteLogMessage(mtDebug, message);
}

// END OF void VapourSynthScriptProcessor::printFrameProps(
//		const VSFrameRef * a_cpFrame)
//==============================================================================

VSNode * VapourSynthScriptProcessor::invokeLibP2PPack(VSNode * a_pVideoNode)
{
    if (a_pVideoNode == nullptr)
        return nullptr;

    VSMap * pArgumentMap = m_cpVSAPI->createMap();

    // packing rgb planar, new for vsapi4
    VSPlugin * pLibp2pPlugin = m_cpVSAPI->getPluginByID(
        "com.djatom.libp2p", m_pCore);

    m_cpVSAPI->mapConsumeNode(pArgumentMap, "clip", a_pVideoNode, maReplace);

    VSMap * pResultMap = m_cpVSAPI->invoke(pLibp2pPlugin, "Pack", pArgumentMap);

    const char * cpResultError = m_cpVSAPI->mapGetError(pResultMap);
    if(cpResultError)
    {
        m_error = tr("Packing RGB24 failed:\n");
        m_error += cpResultError;
        emit signalWriteLogMessage(mtCritical, m_error);
        return nullptr;
    }

    VSNode * pResultNode = m_cpVSAPI->mapGetNode(pResultMap, "clip", 0, nullptr);

    m_cpVSAPI->freeMap(pArgumentMap);
    m_cpVSAPI->freeMap(pResultMap);
    return pResultNode;
}

VSNode *VapourSynthScriptProcessor::invokeResizeConversion(VSNode *a_pVideoNode,
    VSPresetVideoFormat a_id, YuvMatrixCoefficients a_matrixIn)
{
    if (a_pVideoNode == nullptr)
        return nullptr;

    VSMap * pArgumentMap = m_cpVSAPI->createMap();

    auto cpVideoInfo = m_cpVSAPI->getVideoInfo(a_pVideoNode);
    auto cpVideoFormat = cpVideoInfo->format;

    // if input clip is a grayscale, the "matrix" property needs to be remove first
    if (cpVideoFormat.colorFamily == cfGray) {
        m_cpVSAPI->mapConsumeNode(pArgumentMap, "clip", invokeRemoveFrameProps(a_pVideoNode, "_Matrix"), maReplace);
    } else {
        m_cpVSAPI->mapConsumeNode(pArgumentMap, "clip", a_pVideoNode, maReplace);
    }

    VSPlugin * pResizePlugin = m_cpVSAPI->getPluginByID(
        "com.vapoursynth.resize", m_pCore);

    m_cpVSAPI->mapSetInt(pArgumentMap, "format", a_id, maReplace);

    // if clip is yuv, assume rec709
    if (cpVideoFormat.colorFamily == cfYUV) {
        m_cpVSAPI->mapSetInt(pArgumentMap, "matrix_in", int(a_matrixIn), maReplace);

        if(a_matrixIn == YuvMatrixCoefficients::m2020_CL)
        {
            const char * transferIn = "709";
            const char * transferOut = "2020_10";

            m_cpVSAPI->mapSetData(pArgumentMap, "transfer_in_s",
                transferIn, int(strlen(transferIn)), mtVideo, maReplace);
            m_cpVSAPI->mapSetData(pArgumentMap, "transfer_s",
                transferOut, int(strlen(transferOut)), mtVideo, maReplace);
        }
    }

    if (a_id == pfYUV420P8) {
        const char * matrixS = "709";
        m_cpVSAPI->mapSetData(pArgumentMap, "matrix_s", matrixS, int(strlen(matrixS)), mtVideo, maReplace);
    }

    VSMap * pResultMap = m_cpVSAPI->invoke(pResizePlugin, "Point", pArgumentMap);

    const char * cpResultError = m_cpVSAPI->mapGetError(pResultMap);

    if(cpResultError)
    {
        char formatName[32];
        m_cpVSAPI->getVideoFormatName(&m_cpVideoInfo->format, formatName);
        m_error = tr("converting to %1 failed:\n").arg(formatName);
        m_error += cpResultError;
        emit signalWriteLogMessage(mtCritical, m_error);
        return nullptr;
    }

    VSNode * pResultNode = m_cpVSAPI->mapGetNode(pResultMap, "clip", 0, nullptr);

    m_cpVSAPI->freeMap(pArgumentMap);
    m_cpVSAPI->freeMap(pResultMap);
    return pResultNode;
}

VSNode *VapourSynthScriptProcessor::invokeResize(VSNode *a_pVideoNode, int a_width, int a_height)
{
    if (a_pVideoNode == nullptr)
        return nullptr;

    if (a_width <= 0 || a_height <= 0)
        return nullptr;

    VSMap * pArgumentMap = m_cpVSAPI->createMap();

    VSPlugin * pResizePlugin = m_cpVSAPI->getPluginByID(
        "com.vapoursynth.resize", m_pCore);

    m_cpVSAPI->mapConsumeNode(pArgumentMap, "clip", a_pVideoNode, maReplace);
    m_cpVSAPI->mapSetInt(pArgumentMap, "width", a_width, maReplace);
    m_cpVSAPI->mapSetInt(pArgumentMap, "height", a_height, maReplace);

    VSMap * pResultMap = m_cpVSAPI->invoke(pResizePlugin, "Point", pArgumentMap);

    const char * cpResultError = m_cpVSAPI->mapGetError(pResultMap);

    if(cpResultError)
    {
        m_error = tr("resizing U/V plane failed:\n");
        m_error += cpResultError;
        emit signalWriteLogMessage(mtCritical, m_error);
        return nullptr;
    }

    VSNode * pResultNode = m_cpVSAPI->mapGetNode(pResultMap, "clip", 0, nullptr);

    m_cpVSAPI->freeMap(pArgumentMap);
    m_cpVSAPI->freeMap(pResultMap);
    return pResultNode;
}

VSNode *VapourSynthScriptProcessor::invokeRemoveFrameProps(VSNode *a_pVideoNode, std::string a_prop)
{
    if (a_pVideoNode == nullptr)
        return nullptr;

    VSMap * pArgumentMap = m_cpVSAPI->createMap();

    VSPlugin * pStdPlugin = m_cpVSAPI->getPluginByID(
                "com.vapoursynth.std", m_pCore);

    m_cpVSAPI->mapConsumeNode(pArgumentMap, "clip", a_pVideoNode, maReplace);
    const char* propStr = vsedit::strdup(a_prop.c_str());
    m_cpVSAPI->mapSetData(pArgumentMap, "props", propStr, int(strlen(propStr)), mtVideo, maReplace);

    VSMap * pResultMap = m_cpVSAPI->invoke(pStdPlugin, "RemoveFrameProps", pArgumentMap);

    const char * cpResultError = m_cpVSAPI->mapGetError(pResultMap);
    if(cpResultError)
    {
        m_error = tr("remove clip property failed:\n");
        m_error += cpResultError;
        emit signalWriteLogMessage(mtCritical, m_error);
        return nullptr;
    }

    VSNode * pResultNode = m_cpVSAPI->mapGetNode(pResultMap, "clip", 0, nullptr);

    m_cpVSAPI->freeMap(pArgumentMap);
    m_cpVSAPI->freeMap(pResultMap);
    return pResultNode;
}

VSNode *VapourSynthScriptProcessor::invokeShufflePlanes(VSNode *a_pVideoNode, int a_plane)
{
    if (a_pVideoNode == nullptr)
        return nullptr;

    VSMap * pArgumentMap = m_cpVSAPI->createMap();
    VSMap * pResultMap = m_cpVSAPI->createMap();
    const char * cpResultError;

    VSPlugin * pStdPlugin = m_cpVSAPI->getPluginByID(
                "com.vapoursynth.std", m_pCore);

    // remove the "_Matrix" frame property for RGB clip first
    auto videoInfo = m_cpVSAPI->getVideoInfo(a_pVideoNode);
    auto videoFormat = videoInfo->format;

    if (videoFormat.colorFamily == cfRGB) {
        m_cpVSAPI->mapConsumeNode(pArgumentMap, "clip", invokeRemoveFrameProps(a_pVideoNode, "_Matrix"), maReplace);
    } else {
        m_cpVSAPI->mapConsumeNode(pArgumentMap, "clips", a_pVideoNode, maReplace);
    }

    m_cpVSAPI->mapSetInt(pArgumentMap, "planes", a_plane, maReplace);
    m_cpVSAPI->mapSetInt(pArgumentMap, "colorfamily", cfGray, maReplace);

    pResultMap = m_cpVSAPI->invoke(pStdPlugin, "ShufflePlanes", pArgumentMap);

    cpResultError = m_cpVSAPI->mapGetError(pResultMap);
    if(cpResultError)
    {
        m_error = tr("converting to gray scale failed:\n");
        m_error += cpResultError;
        emit signalWriteLogMessage(mtCritical, m_error);
        return nullptr;
    }

    VSNode * pResultNode = m_cpVSAPI->mapGetNode(pResultMap, "clip", 0, nullptr);

    m_cpVSAPI->freeMap(pArgumentMap);
    m_cpVSAPI->freeMap(pResultMap);
    return pResultNode;
}

VSMap *VapourSynthScriptProcessor::invokeSplitPlanes(VSNode *a_pVideoNode)
{
    if (a_pVideoNode == nullptr)
        return nullptr;

    VSMap * pArgumentMap = m_cpVSAPI->createMap();
    VSMap * pResultMap = m_cpVSAPI->createMap();
    const char * cpResultError;

    VSPlugin * pStdPlugin = m_cpVSAPI->getPluginByID(
                "com.vapoursynth.std", m_pCore);

    m_cpVSAPI->mapConsumeNode(pArgumentMap, "clip", a_pVideoNode, maReplace);
    pResultMap = m_cpVSAPI->invoke(pStdPlugin, "SplitPlanes", pArgumentMap);

    cpResultError = m_cpVSAPI->mapGetError(pResultMap);
    if(cpResultError)
    {
        m_error = tr("converting to gray scale failed:\n");
        m_error += cpResultError;
        emit signalWriteLogMessage(mtCritical, m_error);
        return nullptr;
    }

    m_cpVSAPI->freeMap(pArgumentMap);
//    m_cpVSAPI->freeMap(pResultMap);
    return pResultMap;
}

VSNode *VapourSynthScriptProcessor::invokePlaneStats(VSNode *a_pVideoNode)
{
    if (a_pVideoNode == nullptr)
        return nullptr;

    VSMap * pArgumentMap = m_cpVSAPI->createMap();

    VSPlugin * pStdPlugin = m_cpVSAPI->getPluginByID(
                "com.vapoursynth.std", m_pCore);

    m_cpVSAPI->mapConsumeNode(pArgumentMap, "clipa", a_pVideoNode, maReplace);
    VSMap * pResultMap = m_cpVSAPI->invoke(pStdPlugin, "PlaneStats", pArgumentMap);

    const char * cpResultError = m_cpVSAPI->mapGetError(pResultMap);
    if(cpResultError)
    {
        m_error = tr("getting plane stats failed:\n");
        m_error += cpResultError;
        emit signalWriteLogMessage(mtCritical, m_error);
        return nullptr;
    }

    VSNode * pResultNode = m_cpVSAPI->mapGetNode(pResultMap, "clip", 0, nullptr);

    m_cpVSAPI->freeMap(pArgumentMap);
    m_cpVSAPI->freeMap(pResultMap);
    return pResultNode;
}

VSNode *VapourSynthScriptProcessor::invokeAkarinExpr(VSNode *a_pVideoNode, std::string a_expr)
{
    if (a_pVideoNode == nullptr)
        return nullptr;

    VSMap * pArgumentMap = m_cpVSAPI->createMap();

    VSPlugin * pAkarinPlugin = m_cpVSAPI->getPluginByID(
                "info.akarin.vsplugin", m_pCore);

    m_cpVSAPI->mapConsumeNode(pArgumentMap, "clips", a_pVideoNode, maReplace);

    const char* expr = vsedit::strdup(a_expr.c_str()); // convert string to char*
    m_cpVSAPI->mapSetData(pArgumentMap, "expr", expr, strlen(expr), dtBinary, maReplace); // Y plane

    VSMap * pResultMap = m_cpVSAPI->invoke(pAkarinPlugin, "Expr", pArgumentMap);

    const char * cpResultError = m_cpVSAPI->mapGetError(pResultMap);
    if(cpResultError)
    {
        m_error = tr("Akarin Expr failed:\n");
        m_error += cpResultError;
        emit signalWriteLogMessage(mtCritical, m_error);
        return nullptr;
    }

    VSNode * pResultNode = m_cpVSAPI->mapGetNode(pResultMap, "clip", 0, nullptr);

    m_cpVSAPI->freeMap(pArgumentMap);
    m_cpVSAPI->freeMap(pResultMap);
    return pResultNode;
}

VSNode *VapourSynthScriptProcessor::invokeHistogram(VSNode *a_pVideoNode, const QString &a_mode)
{
    if (a_pVideoNode == nullptr)
        return nullptr;

    VSMap * pArgumentMap = m_cpVSAPI->createMap();
    VSPlugin * pHistogramPlugin = m_cpVSAPI->getPluginByID(
                "com.nodame.histogram", m_pCore);

    m_cpVSAPI->mapConsumeNode(pArgumentMap, "clip", a_pVideoNode, maReplace);
    m_cpVSAPI->mapSetInt(pArgumentMap, "histonly", 1, maReplace);

    VSMap * pResultMap = m_cpVSAPI->invoke(pHistogramPlugin, qPrintable(a_mode), pArgumentMap);

    const char * cpResultError = m_cpVSAPI->mapGetError(pResultMap);
    if(cpResultError)
    {
        m_error = tr("Histogram failed:\n");
        m_error += cpResultError;
        emit signalWriteLogMessage(mtCritical, m_error);
        return nullptr;
    }

    VSNode * pResultNode = m_cpVSAPI->mapGetNode(pResultMap, "clip", 0, nullptr);

    m_cpVSAPI->freeMap(pArgumentMap);
    m_cpVSAPI->freeMap(pResultMap);
    return pResultNode;
}

VSNode *VapourSynthScriptProcessor::invokeStackHorizontal(QVector<VSNode*> a_nodes)
{
    if (a_nodes.isEmpty())
        return nullptr;

    VSMap * pArgumentMap = m_cpVSAPI->createMap();
    VSPlugin * pStdPlugin = m_cpVSAPI->getPluginByID(
                "com.vapoursynth.std", m_pCore);

    for (auto node : a_nodes) {
        m_cpVSAPI->mapConsumeNode(pArgumentMap, "clips", node, maAppend);
    }

    VSMap * pResultMap = m_cpVSAPI->invoke(pStdPlugin, "StackHorizontal", pArgumentMap);

    const char * cpResultError = m_cpVSAPI->mapGetError(pResultMap);
    if(cpResultError)
    {
        m_error = tr("StackHorizontal failed:\n");
        m_error += cpResultError;
        emit signalWriteLogMessage(mtCritical, m_error);
        return nullptr;
    }

    VSNode * pResultNode = m_cpVSAPI->mapGetNode(pResultMap, "clip", 0, nullptr);

    m_cpVSAPI->freeMap(pArgumentMap);
    m_cpVSAPI->freeMap(pResultMap);
    return pResultNode;
}
