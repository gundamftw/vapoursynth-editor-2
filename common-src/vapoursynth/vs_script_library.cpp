#include "vs_script_library.h"

#include "../settings/settings_manager_core.h"
#include "../helpers.h"

#include <QSettings>
#include <QProcessEnvironment>

//==============================================================================

void VS_CC vsMessageHandler(int a_msgType, const char * a_message,
	void * a_pUserData)
{
	VSScriptLibrary * pVSScriptLibrary =
		static_cast<VSScriptLibrary *>(a_pUserData);
	pVSScriptLibrary->handleVSMessage(a_msgType, a_message);
}

// END OF void VS_CC vsMessageHandler(int a_msgType, const char * a_message,
//		void * a_pUserData)
//==============================================================================

VSScriptLibrary::VSScriptLibrary(SettingsManagerCore * a_pSettingsManager,
	QObject * a_pParent):
	QObject(a_pParent)
	, m_pSettingsManager(a_pSettingsManager)
	, m_vsScriptLibrary(this)
	, m_vsScriptInitialized(false)
	, m_initialized(false)
    , m_pVSEditCore(nullptr)
    , m_cpVSAPI(nullptr)
    , m_cpVSScriptAPI(nullptr)
{
	Q_ASSERT(m_pSettingsManager);
}

// END OF VSScriptLibrary::VSScriptLibrary(
//		SettingsManagerCore * a_pSettingsManager, QObject * a_pParent)
//==============================================================================

VSScriptLibrary::~VSScriptLibrary()
{
	finalize();
}

// END OF VSScriptLibrary::~VSScriptLibrary()
//==============================================================================

bool VSScriptLibrary::initialize()
{
	if(m_initialized)
		return true;

	bool libraryInitialized = initLibrary();
	if(!libraryInitialized)
		return false;

    m_cpVSScriptAPI = vssGetVSScriptAPI(VSSCRIPT_API_VERSION);

    if(!m_cpVSScriptAPI)
	{
        QString errorString = tr("Failed to initialize "
			"VapourSynth environment!");
		emit signalWriteLogMessage(mtCritical, errorString);
		finalize();
		return false;
	}
	m_vsScriptInitialized = true;

    m_cpVSAPI = m_cpVSScriptAPI->getVSAPI(VAPOURSYNTH_API_VERSION);
	if(!m_cpVSAPI)
	{
        QString errorString = tr("Failed to get VapourSynth API!");
		emit signalWriteLogMessage(mtCritical, errorString);
		finalize();
		return false;
	}

    m_pVSEditCore = m_cpVSAPI->createCore(ccfDisableAutoLoading);

    m_cpVSAPI->addLogHandler(::vsMessageHandler, nullptr,
        static_cast<void *>(this), m_pVSEditCore);

	m_initialized = true;

	return true;
}

// END OF bool VSScriptLibrary::initialize()
//==============================================================================

bool VSScriptLibrary::finalize()
{
    m_cpVSAPI->freeCore(m_pVSEditCore);
	m_cpVSAPI = nullptr;
    m_cpVSScriptAPI = nullptr;

	if(m_vsScriptInitialized)
	{
		m_vsScriptInitialized = false;
	}

	freeLibrary();
	m_initialized = false;

	return true;
}

// END OF bool VSScriptLibrary::finalize()
//==============================================================================

bool VSScriptLibrary::isInitialized() const
{
	return m_initialized;
}

// END OF bool VSScriptLibrary::isInitialized() const
//==============================================================================

const VSAPI * VSScriptLibrary::getVSAPI()
{
	if(!initialize())
		return nullptr;

    return m_cpVSAPI;
}

// END OF const VSAPI * VSScriptLibrary::getVSAPI()
//==============================================================================

const VSSCRIPTAPI * VSScriptLibrary::getVSScriptAPI()
{
    if(!initialize())
        return nullptr;

    return m_cpVSScriptAPI;
}

VSCore *VSScriptLibrary::getVSEditCore()
{
    if(!initialize())
        return nullptr;
    return m_pVSEditCore;
}

bool VSScriptLibrary::initLibrary()
{
	if(m_vsScriptLibrary.isLoaded())
	{
        Q_ASSERT(vssGetVSScriptAPI);
		return true;
	}

	QString libraryName(
#ifdef Q_OS_WIN
        "vsscript"
#else
		"vapoursynth-script"
#endif // Q_OS_WIN
	);

	QString libraryFullPath;
	m_vsScriptLibrary.setFileName(libraryName);
	m_vsScriptLibrary.setLoadHints(QLibrary::ExportExternalSymbolsHint);
	bool loaded = m_vsScriptLibrary.load();

#ifdef Q_OS_WIN
	if(!loaded)
	{
		QSettings settings("HKEY_LOCAL_MACHINE\\SOFTWARE",
			QSettings::NativeFormat);
		libraryFullPath =
			settings.value("VapourSynth/VSScriptDLL").toString();
		if(libraryFullPath.isEmpty())
		{
			libraryFullPath = settings.value(
				"Wow6432Node/VapourSynth/VSScriptDLL").toString();
		}

		if(!libraryFullPath.isEmpty())
		{
			m_vsScriptLibrary.setFileName(libraryFullPath);
			loaded = m_vsScriptLibrary.load();
		}
	}

	if(!loaded)
	{
		QProcessEnvironment environment =
			QProcessEnvironment::systemEnvironment();
		QString basePath;

#ifdef Q_OS_WIN64
        basePath = environment.value("ProgramFiles");
        libraryFullPath = basePath + "\\VapourSynth\\core\\vsscript.dll";
#endif // Q_OS_WIN64

		m_vsScriptLibrary.setFileName(libraryFullPath);
		loaded = m_vsScriptLibrary.load();
	}
#endif // Q_OS_WIN

	if(!loaded)
	{
		QStringList librarySearchPaths =
			m_pSettingsManager->getVapourSynthLibraryPaths();
		for(const QString & path : librarySearchPaths)
		{
			libraryFullPath = vsedit::resolvePathFromApplication(path) +
				QString("/") + libraryName;
			m_vsScriptLibrary.setFileName(libraryFullPath);
			loaded = m_vsScriptLibrary.load();
			if(loaded)
				break;
		}
	}

	if(!loaded)
	{
		emit signalWriteLogMessage(mtCritical,
			"Failed to load vapoursynth script library!\n"
			"Please set up the library search paths in settings.");
		return false;
	}

	struct Entry
	{
		QFunctionPointer * ppFunction;
		const char * name;
		const char * fallbackName;
	};

    /* VSAPI 4 only needs the getVSScriptAPI() entry point, as
     * all vsscript functions can be called within the VSScriptAPI now  */
	Entry vssEntries[] =
	{
        {reinterpret_cast<QFunctionPointer *>(&vssGetVSScriptAPI), "getVSScriptAPI",
            "_getVSScriptAPI@0"}
    };

    // rename vss function pointers for vs editor
	for(Entry & entry : vssEntries)
	{
		Q_ASSERT(entry.ppFunction);
		*entry.ppFunction = m_vsScriptLibrary.resolve(entry.name);
		if(!*entry.ppFunction)
		{ // Win32 fallback
			*entry.ppFunction = m_vsScriptLibrary.resolve(entry.fallbackName);
		}
		if(!*entry.ppFunction)
		{
            QString errorString = tr("Failed to get entry %1() "
                "in vapoursynth script library!").arg(entry.name);
			emit signalWriteLogMessage(mtCritical, errorString);
			freeLibrary();
			return false;
		}
	}

	return true;
}

// END OF bool VSScriptLibrary::initLibrary()
//==============================================================================

void VSScriptLibrary::freeLibrary()
{
    vssGetVSScriptAPI = nullptr;

	if(m_vsScriptLibrary.isLoaded())
		m_vsScriptLibrary.unload();
}

// END OF void VSScriptLibrary::freeLibrary()
//==============================================================================

void VSScriptLibrary::handleVSMessage(int a_messageType,
	const QString & a_message)
{
	emit signalWriteLogMessage(a_messageType, a_message);
}

// END OF void VSScriptLibrary::handleVSMessage(int a_messageType,
//		const QString & a_message)
//==============================================================================
