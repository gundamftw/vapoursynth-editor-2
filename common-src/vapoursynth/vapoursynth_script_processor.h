#ifndef VAPOURSYNTHSCRIPTPROCESSOR_H
#define VAPOURSYNTHSCRIPTPROCESSOR_H

#include "vs_script_processor_structures.h"
#include "../settings/settings_manager_core.h"

#include <QObject>
#include <QVector>
#include <deque>
#include <vector>
#include <map>

class VSScriptLibrary;

//==============================================================================

class VapourSynthScriptProcessor : public QObject
{
	Q_OBJECT

public:

	VapourSynthScriptProcessor(SettingsManagerCore * a_pSettingsManager,
		VSScriptLibrary * a_pVSScriptLibrary, QObject * a_pParent = nullptr);

    virtual ~VapourSynthScriptProcessor() override;

	bool initialize(const QString& a_script, const QString& a_scriptName);

	bool finalize();

	bool isInitialized() const;

	QString error() const;

    const VSVideoInfo * getVideoInfo();

	bool requestFrameAsync(int a_frameNumber, int a_outputIndex = 0,
        bool a_needPreview = false, bool a_isPlaying = false, int a_scopeNum = 0,
        PreviewFilters a_previewFilters = PreviewFilters());

	bool flushFrameTicketsQueue();

    void flushNodePairMap();

	const QString & script() const;

	const QString & scriptName() const;

	void setScriptName(const QString & a_scriptName);

    QString framePropsString(const VSFrame * a_cpFrame) const;

public slots:

    void slotApplyPreviewFilters(PreviewFilters & a_previewFilters);

signals:

	void signalWriteLogMessage(int a_messageType, const QString & a_message);

	void signalDistributeFrame(int a_frameNumber, int a_outputIndex,
        const VSFrame * a_cpOutputFrame, const VSFrame * a_cpPreviewFrame,
        const VSFrame * a_cpScopeFrame = nullptr);

	void signalFrameRequestDiscarded(int a_frameNumber, int a_outputIndex,
		const QString & a_reason);

	void signalFrameQueueStateChanged(size_t a_inQueue, size_t a_inProcess,
		size_t a_maxThreads);

	void signalFinalized();

private slots:

	void slotReceiveFrameAndProcessQueue(
        const VSFrame * a_cpFrame, int a_frameNumber,
        VSNode * a_pNode, QString a_errorMessage);

private:

    void receiveFrame(const VSFrame * a_cpFrame, int a_frameNumber,
        VSNode * a_pNode, const QString & a_errorMessage);

	void processFrameTicketsQueue();

	void sendFrameQueueChangeSignal();

    bool recreatePreviewNode(NodePair & a_nodePair, PreviewFilters a_previewFilters);
    bool createWaveformNode(NodePair &a_nodePair);
    bool createRGBParadeNode(NodePair &a_nodePair);

	void freeFrameTicket(FrameTicket & a_ticket);

    NodePair & getNodePair(int a_outputIndex, bool a_needPreview, PreviewFilters a_previewFilters);


    void printFrameProps(const VSFrame * a_cpFrame);

    /* invoke vapoursynth plugin functions */
    VSNode * invokeLibP2PPack(VSNode * a_pVideoNode);
    VSNode * invokeResizeConversion(VSNode * a_pVideoNode,
        VSPresetVideoFormat a_id, YuvMatrixCoefficients a_matrixIn = YuvMatrixCoefficients::m709);
    VSNode * invokeResize(VSNode * a_pVideoNode, int a_width, int a_height);
    VSNode * invokeRemoveFrameProps(VSNode *a_pVideoNode, std::string a_prop);
    VSNode * invokeShufflePlanes(VSNode * a_pVideoNode, int a_plane);
    VSMap * invokeSplitPlanes(VSNode * a_pVideoNode);
    VSNode * invokePlaneStats(VSNode * a_pVideoNode);
    VSNode * invokeAkarinExpr(VSNode * a_pVideoNode, std::string a_expr);
    VSNode * invokeHistogram(VSNode *a_pVideoNode, const QString &a_mode);
    VSNode * invokeStackHorizontal(QVector<VSNode*> a_nodes);

	SettingsManagerCore * m_pSettingsManager;

	VSScriptLibrary * m_pVSScriptLibrary;

	QString m_script;

	QString m_scriptName;

	QString m_error;

	bool m_initialized;

    bool m_finalizing;

    VSCore * m_pCore;

	const VSAPI * m_cpVSAPI;
    const VSSCRIPTAPI * m_cpVSScriptAPI;

	VSScript * m_pVSScript;

    const VSVideoInfo * m_cpVideoInfo;
    VSCoreInfo m_cpCoreInfo;

	std::deque<FrameTicket> m_frameTicketsQueue;
    QVector<FrameTicket> m_frameTicketsInProcess;
    std::map<int, NodePair> m_nodePairMap;

	ResamplingFilter m_chromaResamplingFilter;
	ChromaPlacement m_chromaPlacement;
	double m_resamplingFilterParameterA;
	double m_resamplingFilterParameterB;
	YuvMatrixCoefficients m_yuvMatrix;
};

//==============================================================================

#endif // VAPOURSYNTHSCRIPTPROCESSOR_H
