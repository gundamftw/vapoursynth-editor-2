#ifndef VS_SCRIPT_PROCESSOR_STRUCTURES_H_INCLUDED
#define VS_SCRIPT_PROCESSOR_STRUCTURES_H_INCLUDED

#include <vapoursynth/VSScript4.h>
#include "../settings/settings_definitions_core.h"

//==============================================================================

struct Frame
{
    int number;
    int outputIndex;
    const VSFrame * cpOutputFrame;
    const VSFrame * cpPreviewFrame;
    const VSFrame * cpWaveformFrame;
    const VSFrame * cpRGBParadeFrame;

    Frame(int a_number, int a_outputIndex,
          const VSFrame * a_cpOutputFrame,
          const VSFrame * a_cpPreviewFrame = nullptr,
          const VSFrame * a_cpWaveformFrame = nullptr,
          const VSFrame * a_cpRGBParadeFrame = nullptr);
    bool operator==(const Frame & a_other) const;
};

//==============================================================================

struct PreviewFilters
{
    YuvMatrixCoefficients colorMatrix;
    bool planeActive;
    YUVPlane plane;
    bool updateFilters;

    PreviewFilters();
    PreviewFilters(YuvMatrixCoefficients a_yuvMatrixCoefficient, bool a_planeActive, YUVPlane a_plane);
    bool operator==(const PreviewFilters & a_other) const;
    bool operator!=(const PreviewFilters & a_other) const;
};

//==============================================================================

struct FrameTicket
{
    int frameNumber;
    int outputIndex;
    VSNode * pOutputNode;
    bool needPreview;
    bool playing;
    int scopeNum;
    VSNode * pPreviewNode;
    VSNode * pWaveformNode;
    VSNode * pRGBParadeNode;
    const VSFrame * cpOutputFrame;
    const VSFrame * cpPreviewFrame;
    const VSFrame * cpScopeFrame;
    PreviewFilters previewFilters;
    bool discard;

    FrameTicket();
	FrameTicket(int a_frameNumber, int a_outputIndex,
        VSNode * a_pOutputNode, bool a_needPreview = false,
        bool a_playing = false, int a_scopeNum = 0,
        VSNode * a_pPreviewNode = nullptr, VSNode *a_pWaveformNode = nullptr,
        VSNode * a_pRGBParadeNode = nullptr, PreviewFilters a_previewFilters = {});

    bool isComplete() const;
    bool isPlaying() const;
};

//==============================================================================

struct NodePair
{
    int outputIndex;
    VSNode * pOutputNode;
    VSNode * pPreviewNode;
    VSNode * pWaveformNode;
    VSNode * pRGBParadeNode;

    NodePair();
    NodePair(int a_outputIndex, VSNode * a_pOutputNode,
             VSNode * a_pPreviewNode);

    bool isNull() const;
    bool isValid() const;
};

//==============================================================================

#endif // VS_SCRIPT_PROCESSOR_STRUCTURES_H_INCLUDED
